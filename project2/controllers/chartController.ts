import {Request,Response} from 'express';
import {ChartService} from '../services/chartService';

export class ChartController{
    constructor(private chartService:ChartService){};

    getDistrct = async (req:Request,res:Response)=>{
        try{
            const result = await this.chartService.getDistrictChart();
            res.json(result)
        }catch(err){
            console.error(err)
        }
    };

    getSalary = async (req:Request,res:Response)=>{
        try{
            const result = await this.chartService.getSalaryChart();
            res.json(result)
        }catch(err){
            console.error(err)
        }
    };

    getSource = async (req:Request,res:Response)=>{
        try{
            const result = await this.chartService.getSourceChart();
            res.json(result)
        }catch(err){
            console.error(err)
        }
    };

}