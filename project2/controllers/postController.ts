import {Request,Response} from 'express';
import {PostService,RecommendPostService} from '../services/postService';

export class PostController{
    constructor(private postService:PostService){};

    get = async (req:Request,res:Response)=>{
        try{
            const data = await req.query
            const postResults = await this.postService.getPost(data);
            res.json(postResults)
        }catch(err){
            console.error(err)
        }
    };
}

export class RecommendPostsController{
    constructor(private recommendPostService:RecommendPostService){};

    get = async (req:Request,res:Response)=>{
        try{
            const newPosts = await this.recommendPostService.getNewPost();
            res.json(newPosts)
        }catch(err){
            console.error(err)
        }
    };
}