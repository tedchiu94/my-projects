import * as Knex from "knex";

export async function up(knex: Knex): Promise<any> {
    const hasPostTable = await knex.schema.hasTable('post');
    if(!hasPostTable){
        await knex.schema.createTable('post', table => {
            table.increments();
            table.string("job_position").notNullable();
            table.string("company").notNullable();
            table.string("district").notNullable();
            table.string("salary").notNullable();
            table.string("resource_from").notNullable();
            table.string("href").notNullable();
            table.string("description")
            table.timestamps(false, true);
        });
    }
    const hasRecommenderTable = await knex.schema.hasTable('recommender');
    if(!hasRecommenderTable){
        await knex.schema.createTable('recommender', table => {
            table.increments();
            table.string("job_position").notNullable();
            table.string("company").notNullable();
            table.string("district").notNullable();
            table.string("salary").notNullable();
            table.string("resource_from").notNullable();
            table.string("href").notNullable();
            table.string("description").notNullable();
            table.integer("recommender_one").notNullable();
            table.integer("recommender_two").notNullable();
            table.integer("recommender_three").notNullable();
            table.integer("recommender_four").notNullable();
            table.timestamps(false, true);
        })
    }
}

export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTableIfExists('post');
}

