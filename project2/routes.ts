import express from 'express';
import {postController,recommendPostController, chartController} from './main';

export const routes = express.Router();

routes.get('/search',postController.get);
routes.get('/newPosts',recommendPostController.get);

routes.get('/district',chartController.getDistrct);
routes.get('/salary',chartController.getSalary);
routes.get('/source',chartController.getSource);