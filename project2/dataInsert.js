// const puppeteer = require('puppeteer');
const jsonfile = require("jsonfile");

const pg = require('pg');
const dotenv = require('dotenv');
const { json } = require("express");
dotenv.config();
const client = new pg.Client({
    database:process.env.DB_NAME,
    user:process.env.DB_USERNAME,
    password:process.env.DB_PASSWORD,
    host:"localhost",
    port:5432
});

// insert into job market to DB
(async () => {
    await client.connect();

    const recommenderInsert = async function (jsonPath){
        const recommenderPosts = await jsonfile.readFile(jsonPath);
        for(let post of recommenderPosts){
            await client.query(`INSERT INTO "recommender" 
                            (job_position,company,district,salary,description,href,resource_from,recommender_one,recommender_two,recommender_three,recommender_four) 
                                values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)`,
                            [post.job_position,post.company,post.district,post.salary,
                                post.description,post.href,post.resource_from,
                                post.recommender_one,post.recommender_two,post.recommender_three,post.recommender_four]);
        };
    };
    const startInsert = async function (jsonPath) {
        const lowerTwentyKPosts = await jsonfile.readFile(jsonPath);    
        for(let post of lowerTwentyKPosts){
            await client.query(`INSERT INTO "post" (job_position,company,district,salary,href,resource_from) values($1,$2,$3,$4,$5,$6)`,
                                [post.job_position,post.company,post.district,post.salary,
                                    post.href,post.resource_from]);
        };
    };
    
    const changeSalary = async function (jsonPath,newSalary) {
        const twentyTothirtyKPosts = await jsonfile.readFile(jsonPath);
        for(let post of twentyTothirtyKPosts){    
            await client.query(`update post set salary = $1 where (job_position = $2) and (company = $3)`,
                                [newSalary,post.job_position,post.company]);      
        };
    };
    
    const insertData = async function (jsonPath) {
        const twentyTothirtyKPosts = await jsonfile.readFile(jsonPath);
        for(let post of twentyTothirtyKPosts){            
            await client.query(`INSERT INTO post (job_position,company,district,salary,href,resource_from) values($1,$2,$3,$4,$5,$6)`,
            [post.job_position,post.company,post.district,
                post.salary,post.href,post.resource_from]); 
            };
        };
        
    const deleteRepeatData = async function (jsonPath,middleSalary,jsonSalary) {
        const twentyTothirtyKPosts = await jsonfile.readFile(jsonPath);
        const twentyKPosts = await client.query(`select * from post where salary = $1`,[middleSalary]);
        for(let jsPost of twentyKPosts.rows){        
            const strJsPostJobPosition = jsPost.job_position;
            const strJsPostCompany = jsPost.company;
            
            await client.query(`delete from post where (salary = $1) and (job_position = $2) and (company = $3)`,
            [jsonSalary,strJsPostJobPosition,strJsPostCompany]); 
        };
    };  
    // recommenderPosts
    await recommenderInsert('./jobsData_new/recommender/recommender2.json');
    //jobMarket
    const jobMarketJsonToDb = async function (jsonPathTwentyK,jsonPathTwentyOneK,jsonPathThirtyK) {
        //<20k
        await startInsert(jsonPathTwentyK);
        //20-30k 
        await changeSalary(jsonPathTwentyOneK,'20k');
        await insertData(jsonPathTwentyOneK);
        await deleteRepeatData(jsonPathTwentyOneK,'20k','$21k - $30k');
        //30-40k
        await changeSalary(jsonPathThirtyK,'30k');
        await insertData(jsonPathThirtyK);
        await deleteRepeatData(jsonPathThirtyK,'30k','$31k - $40k');        
    };
    //jobsDb
    const jobsdbJsonToDb = async function (jsonPathTwentyK,jsonPathTwentyOneK,jsonPathThirtyK) {
        await startInsert(jsonPathTwentyK);
        //20-30k 
        await changeSalary(jsonPathTwentyOneK,'20k');
        await insertData(jsonPathTwentyOneK);
        await deleteRepeatData(jsonPathTwentyOneK,'20k','$20k - $30k');
        //30-40k
        await changeSalary(jsonPathThirtyK,'30k');
        await insertData(jsonPathThirtyK);
        await deleteRepeatData(jsonPathThirtyK,'30k','$30k - $40k');  
    }
    
    //hkIsland
    await jobMarketJsonToDb('./jobsData_new/jobMarket/hkIsland_20k.json',
                            './jobsData_new/jobMarket/hkIsland_21k-30k.json',
                            './jobsData_new/jobMarket/hkIsland_31k-40k.json');
    await jobsdbJsonToDb('./jobsData_new/jobsDb/hkIsland_20k.json',
                            './jobsData_new/jobsDb/hkIsland_20k-30k.json',
                            './jobsData_new/jobsDb/hkIsland_30k-40k.json');               
    // kowloon
    await jobMarketJsonToDb('./jobsData_new/jobMarket/kowloon_20k.json',
                            './jobsData_new/jobMarket/kowloon_21k-30k.json',
                            './jobsData_new/jobMarket/kowloon_31k-40k.json');
    await jobsdbJsonToDb('./jobsData_new/jobsDb/kowloon_20k.json',
                            './jobsData_new/jobsDb/kowloon_20k-30k.json',
                            './jobsData_new/jobsDb/kowloon_30k-40k.json');  
    //STTP
    await jobMarketJsonToDb('./jobsData_new/jobMarket/STTP_20k.json',
                            './jobsData_new/jobMarket/STTP_21k-30k.json',
                            './jobsData_new/jobMarket/STTP_31k-40k.json');
    await jobsdbJsonToDb('./jobsData_new/jobsDb/STTP_20k.json',
                            './jobsData_new/jobsDb/STTP_20k-30k.json',
                            './jobsData_new/jobsDb/STTP_30k-40k.json');  
    //TWKT
    await jobMarketJsonToDb('./jobsData_new/jobMarket/TWKT_20k.json',
                            './jobsData_new/jobMarket/TWKT_21k-30k.json',
                            './jobsData_new/jobMarket/TWKT_31k-40k.json');
    await jobsdbJsonToDb('./jobsData_new/jobsDb/TWKT_20k.json',
                            './jobsData_new/jobsDb/TWKT_20k-30k.json',
                            './jobsData_new/jobsDb/TWKT_30k-40k.json');
    await client.end();
})();