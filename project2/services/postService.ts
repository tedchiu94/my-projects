// import {Client} from 'pg';
import Knex from 'knex';

export class PostService{
    constructor(private knex:Knex){};

    async getPost(data:any){
        const {district,salary,source} =  await data;
        let sql = 'select * from post where true';
        let binding = [];
        if(district){
            sql += ' and district = (?)'
            binding.push(district)
        }
        if (salary) {
            sql += ' and salary = (?)' 
            binding.push(salary)
        }
        if (source) {
            sql += ' and resource_from = (?)'
            binding.push(source)
        }
        // const postResults = await this.clinet.query(sql, binding);
        const postResults = await this.knex
        .raw(sql,binding)
        
        // console.log(postResults.rows)                                
        // console.log(sql)
        return postResults.rows;
    }
}

export class RecommendPostService{
    constructor(private knex:Knex){};

    async getNewPost(){
        // const newPost = await this.clinet.query(`select * from post order by id desc`);
        // const newPost = await this.knex
        //                             .select('*')
        //                             .from('post')
        //                             .orderBy('id', 'desc')
        const randomPost = Math.floor(Math.random()*28)
        const newPost = await this.knex
                                    .select('*')
                                    .from('recommender')
                                    .where('id',randomPost)
        const recommenderOnePostId = {
            id:newPost[0]['recommender_one'] + 1
        };
        const recommenderTwoPostId = {
            id:newPost[0]['recommender_two'] + 1
        };
        const recommenderthreePostId = {
            id:newPost[0]['recommender_three'] + 1
        };
        const recommenderFourPostId = {
            id:newPost[0]['recommender_four'] + 1
        };

        const recommenderPosts = await this.knex
                                            .select('*')
                                            .from('recommender')
                                            .where(recommenderOnePostId)
                                            .orWhere(recommenderTwoPostId)
                                            .orWhere(recommenderthreePostId)
                                            .orWhere(recommenderFourPostId)
        // //python order start from zero, DB id start from 1
        const recommenderOne = newPost[0]['recommender_one'] + 1
        const recommenderTwo = newPost[0]['recommender_two'] + 1
        const recommenderThree = newPost[0]['recommender_three'] + 1
        const recommenderFour = newPost[0]['recommender_four'] + 1
        console.log(`Test post: ${randomPost}`)
        console.log(`recommender posts index number: ${newPost[0]['recommender_one']},${newPost[0]['recommender_two']},${newPost[0]['recommender_three']},${newPost[0]['recommender_four']}`)
        console.log(`Post id: ${recommenderOne},${recommenderTwo},${recommenderThree},${recommenderFour}`)
        
        return recommenderPosts
    }
}