import Knex from 'knex';
const knexfile = require('../knexfile');
const knex = Knex(knexfile["test"]);
import {PostService} from './postService';

describe("postS",()=>{

    let postService:PostService;
    const table = 'post'

    beforeEach(async ()=>{
        postService = new PostService(knex);
        await knex(table).del();
        await knex.insert({
            job_position:"test",
            company:"test",
            district:"test",
            salary:"test",
            resource_from:"test",
            href:"test",
            description:"test"
        }).into(table);
    })

    it("should get all posts",async ( )=>{
        const posts = await postService.getPost('test');
        expect(posts.length).toBe(1);
    });

    afterAll(()=>{
        knex.destroy();
    });
})