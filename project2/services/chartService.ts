// import { Client } from 'pg';
import Knex from 'knex';

export class ChartService {
    constructor(private knex:Knex) { };

    async getDistrictChart() {
        // const hkIslandPostResults = await this.clinet.query(`select * from post where (district = $1)`,
        //     ['HK Island']);
        const hkIslandPostResults = await this.knex
                                                .count('*')
                                                .from('post')
                                                .where('district','HK Island');

        // const kowloonPostResults = await this.clinet.query(`select * from post where (district = $1)`,
        //     ['Kowloon']);
        const kowloonPostResults = await this.knex
                                                .count('*')
                                                .from('post')
                                                .where('district','Kowloon');

        // const sttpPostResults = await this.clinet.query(`select * from post where (district = $1)`,
        //     ['ST TP']);
        const sttpPostResults = await this.knex
                                            .count('*')
                                            .from('post')
                                            .where('district','ST TP');

        // const twktPostResults = await this.clinet.query(`select count(*) as count from post where (district = $1)`,
        //     ['TW KT']);
        const twktPostResults = await this.knex
                                            .count('*')
                                            .from('post')
                                            .where('district','TW KT');
        
        const postArr = [];
        postArr.push(hkIslandPostResults[0]['count'],
            kowloonPostResults[0]['count'],
            sttpPostResults[0]['count'],
            twktPostResults[0]['count'])
        return postArr
    };

    async getSalaryChart(){
        // const lessthan20kPostResults = await this.clinet.query(`select * from post where (salary = $1)`,
        //     ['< $20k']);
        const lessthan20kPostResults = await this.knex
                                                    .count('*')
                                                    .from('post')
                                                    .where('salary','< $20k');

        // const equal20kPostResults = await this.clinet.query(`select * from post where (salary = $1)`,
        //     ['20k']);
        const equal20kPostResults = await this.knex
                                                .count('*')
                                                .from('post')
                                                .where('salary','20k');

        // const between20k30kPostResults = await this.clinet.query(`select * from post where (salary = $1)`,
        //     ['$20k - $30k']);
        const between20k30kPostResults = await this.knex
                                                    .count('*')
                                                    .from('post')
                                                    .where('salary','$20k - $30k');

        // const between21k30kPostResults = await this.clinet.query(`select * from post where (salary = $1)`,
        //     ['$21k - $30k']);
        const between21k30kPostResults = await this.knex
                                                    .count('*')
                                                    .from('post')
                                                    .where('salary','$21k - $30k');

        // const equal30kPostResults = await this.clinet.query(`select * from post where (salary = $1)`,
        //     ['30k']);
        const equal30kPostResults = await this.knex
                                                .count('*')
                                                .from('post')
                                                .where('salary','30k');

        // const between30k40kPostResults = await this.clinet.query(`select * from post where (salary = $1)`,
        //     ['$30k - $40k']);
        const between30k40kPostResults = await this.knex
                                                    .count('*')
                                                    .from('post')
                                                    .where('salary','$30k - $40k');
        
        // const between31k40kPostResults = await this.clinet.query(`select * from post where (salary = $1)`,
        //     ['$31k - $40k']);
        const between31k40kPostResults = await this.knex
                                                    .count('*')
                                                    .from('post')
                                                    .where('salary','$31k - $40k');

        const postArr = [];
        const numOfbetween20k30kPostResults:number = +between20k30kPostResults[0]['count']
        const numOfequal20kPostResults:number = +equal20kPostResults[0]['count']
        const numOfbetween21k30kPostResults:number = +between21k30kPostResults[0]['count']

        const numOfbetween30k40kPostResults:number = +between30k40kPostResults[0]['count']
        const numOfequal30kPostResults:number = +equal30kPostResults[0]['count']
        const numOfbetween31k40kPostResults:number = +between31k40kPostResults[0]['count']

        // console.log(numOfbetween20k30kPostResults + numOfequal20kPostResults + numOfbetween21k30kPostResults)
        postArr.push(lessthan20kPostResults[0]['count'],
                    numOfbetween20k30kPostResults + numOfequal20kPostResults + numOfbetween21k30kPostResults,
                    numOfbetween30k40kPostResults + numOfequal30kPostResults + numOfbetween31k40kPostResults)
        return postArr
    };

    async getSourceChart() {
        // const jobMarketPostResults = await this.clinet.query(`select * from post where (resource_from = $1)`,
        //     ['jobMarket']);
        const jobMarketPostResults = await this.knex
                                                .count('*')
                                                .from('post')
                                                .where('resource_from','jobMarket');
        // const jobsDBPostResults = await this.clinet.query(`select * from post where (resource_from = $1)`,
        //     ['jobsDB']);
        const jobsDBPostResults = await this.knex
                                                .count('*')
                                                .from('post')
                                                .where('resource_from','jobsDB');
   
    const postArr = [];
    postArr.push(jobMarketPostResults[0]['count'],
                jobsDBPostResults[0]['count'])

        return postArr
    };

}