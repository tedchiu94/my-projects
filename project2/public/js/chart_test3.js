
let arr = []

async function loadChartResults() {
    const res = await fetch('/district');
    const arrPosts = await res.json();
    // const arrPostsText = await res.text();
    for (let arrPost of arrPosts) {
        arr.push(arrPost)
    }
}

loadChartResults();

let arrSalary = []

async function loadChartSalaryResults() {
    const res = await fetch('/salary');
    const arrPosts = await res.json();
    // const arrPostsText = await res.text();
    for (let arrPost of arrPosts) {
        arrSalary.push(arrPost)
    }
}

loadChartSalaryResults();

let arrSource = []

async function loadChartSourceResults() {
    const res = await fetch('/source');
    const arrPosts = await res.json();
    // const arrPostsText = await res.text();
    for (let arrPost of arrPosts) {
        arrSource.push(arrPost)
    }
}

loadChartSourceResults();


function displayDistrict(){
    jobMarket.data.labels= ['HK Island', 'Kowloon', 'ST TP', 'TW KT'];
    jobMarket.data.datasets[0].data = [arr[0],arr[1],arr[2],arr[3]];

    jobMarket.update();
}

function displaySalary(){
    jobMarket.data.labels= ['< 20k', '$20k - $29k', '$30k - $40k'];
    jobMarket.data.datasets[0].data = [arrSalary[0],arrSalary[1],arrSalary[2]];

    jobMarket.update();
}

function displaySource(){
    jobMarket.data.labels= ['jobMarket', 'jobsDB'];
    jobMarket.data.datasets[0].data = [arrSource[0],arrSource[1]];

    jobMarket.update();
}



// chart.defaults.global.defaultFontFamily = 'Lato';
// chart.defaults.global.defaultFontSize = 18;
// chart.defaults.global.defaultFontColor = '#777';

var charts = document.getElementById('myChart').getContext('2d');
var jobMarket = new Chart(charts, {
    type: 'bar', //bar, horizontalBar, pie, line, doughnut, radar, polarArea
    data: {
        labels: [],
        datasets: [{
            label: 'Job Openings',
            data: [],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderWidth: 1,
            borderColor: '#777',
            hoverBorderWidth: 2,
            hoverBorderColor: '#000',
        }]
    },
    options: {
        cornerRadius: 10,
        responsive: true,
        maintainAspectRatio: false,
        title: {
            // display: true,
            text: 'I.T. Job Market in HK ',
            fontSize: 20,
        },
        events: ['click'],
        legend: {
            display: false,
            position: 'right',
            labels: {
                fontColor: '#000',
                fontFamily: 'Lato',
                fontSize: 50,
            }
        },
        // layout: {
        //     padding: {
        //         left: 50,
        //         right: 0,
        //         bottom: 0,
        //         top: 0,
        //     }
        // },
        tooltips: {
            // enabled:false
        },
        scales: {
            yAxes: [{
                ticks: {
                    max:250,
                    beginAtZero: true,
                    fontSize: 30,

                }
            }],
            xAxes: [{
                ticks: {
                    fontSize: 30,

                }
            }]
        }
    }
});





































////////////////////////////////////////////////////////////////
// var charts = document.getElementById('myChart').getContext('2d');
// var jobMarket = new Chart(charts,{                
//     type: 'bar',
//     data: {
//         labels: [],
//         datasets: [{
//             label: 'Number of posts',
//             data: [10 , 20, 30 ],
//             backgroundColor: [
//                 'rgba(255, 99, 132, 0.2)',
//                 'rgba(54, 162, 235, 0.2)',
//                 'rgba(255, 206, 86, 0.2)',
//                 'rgba(75, 192, 192, 0.2)',
//                 // 'rgba(153, 102, 255, 0.2)',
//                 // 'rgba(255, 159, 64, 0.2)'
//             ],
//             borderColor: [
//                 'rgba(255, 99, 132, 1)',
//                 'rgba(54, 162, 235, 1)',
//                 'rgba(255, 206, 86, 1)',
//                 'rgba(75, 192, 192, 1)',
//                 // 'rgba(153, 102, 255, 1)',
//                 // 'rgba(255, 159, 64, 1)'
//             ],
//             borderWidth: 5 
//         }]
//     },
    // options: {
    //     scales: {
    //         yAxes: [{
    //             ticks: {
    //                 beginAtZero: true
    //             }
    //         }]
    //     }
    // }

//     });
// function displayDistrict(){
//     // jobMarket.type = {{bar}};
//     jobMarket.data.datasets[0].data = [arr[0],arr[1],arr[2],arr[3]];
//         jobMarket.data.labels= ['HK Island', 'Kowloon', 'ST TP', 'TW KT'];

// // 	// myLineChart.data.datasets[0].data= [arr[0],arr[1],arr[2],arr[3]];

//     jobMarket.update()

// }




















// let ctx = document.getElementById('myChart1');

//             let myChart = new Chart(ctx, {
//                 type: 'pie',
//                 data: {
//                     labels: ['HK Island', 'Kowloon', 'ST TP', 'TW KT'],
//                     datasets: [{
//                         label: 'Number of posts',
//                         data: [arr[0],arr[1],arr[2],arr[3]],
//                         backgroundColor: [
//                             'rgba(255, 99, 132, 0.2)',
//                             'rgba(54, 162, 235, 0.2)',
//                             'rgba(255, 206, 86, 0.2)',
//                             'rgba(75, 192, 192, 0.2)',
//                             // 'rgba(153, 102, 255, 0.2)',
//                             // 'rgba(255, 159, 64, 0.2)'
//                         ],
//                         borderColor: [
//                             'rgba(255, 99, 132, 1)',
//                             'rgba(54, 162, 235, 1)',
//                             'rgba(255, 206, 86, 1)',
//                             'rgba(75, 192, 192, 1)',
//                             // 'rgba(153, 102, 255, 1)',
//                             // 'rgba(255, 159, 64, 1)'
//                         ],
//                         borderWidth: 5 
//                     }]
//                 },
//                 options: {
//                     scales: {
//                         yAxes: [{
//                             ticks: {
//                                 beginAtZero: true
//                             }
//                         }]
//                     }
//                 }
//             });
//         })
















// var canvas = document.getElementById('myChart');
// var myLineChart = Chart(canvas,{                
//     type: 'pie',
//     data: {
//         labels: ['HK Island', 'Kowloon', 'ST TP', 'TW KT'],
//         datasets: [{
//             label: 'Number of posts',
//             data: [arr[0],arr[1],arr[2],arr[3]],
//             backgroundColor: [
//                 'rgba(255, 99, 132, 0.2)',
//                 'rgba(54, 162, 235, 0.2)',
//                 'rgba(255, 206, 86, 0.2)',
//                 'rgba(75, 192, 192, 0.2)',
//                 // 'rgba(153, 102, 255, 0.2)',
//                 // 'rgba(255, 159, 64, 0.2)'
//             ],
//             borderColor: [
//                 'rgba(255, 99, 132, 1)',
//                 'rgba(54, 162, 235, 1)',
//                 'rgba(255, 206, 86, 1)',
//                 'rgba(75, 192, 192, 1)',
//                 // 'rgba(153, 102, 255, 1)',
//                 // 'rgba(255, 159, 64, 1)'
//             ],
//             borderWidth: 5 
//         }]
//     },
//     options: {
//         scales: {
//             yAxes: [{
//                 ticks: {
//                     beginAtZero: true
//                 }
//             }]
//         }
//     }

//     });


// function displayDistrict(){
//     myLineChart.data.labels= ['HK Island', 'Kowloon', 'ST TP', 'TW KT'];

// 	// myLineChart.data.datasets[0].data= [arr[0],arr[1],arr[2],arr[3]];
// //   myLineChart.data.labels[7] = "Newly Added";
//   myLineChart.update();
// }



// var option = {
// 	showLines: true
// };
// var myLineChart = Chart(canvas,{

//     data:data,
//   options:option,

// });









































// var canvas = document.getElementById('myChart');


// var data = {
//     labels: [],
//     datasets: [
//         {
//             label: "My First dataset",
//             fill: false,
//             lineTension: 0.1,
//             backgroundColor: "rgba(75,192,192,0.4)",
//             borderColor: "rgba(75,192,192,1)",
//             borderCapStyle: 'butt',
//             borderDash: [],
//             borderDashOffset: 0.0,
//             borderJoinStyle: 'miter',
//             pointBorderColor: "rgba(75,192,192,1)",
//             pointBackgroundColor: "#fff",
//             pointBorderWidth: 1,
//             pointHoverRadius: 5,
//             pointHoverBackgroundColor: "rgba(75,192,192,1)",
//             pointHoverBorderColor: "rgba(220,220,220,1)",
//             pointHoverBorderWidth: 2,
//             pointRadius: 5,
//             pointHitRadius: 10,
//             data: [65, 59, 80, 0, 56, 55, 40],
//         }
//     ]
// };

// function displayDistrict(){
//     myLineChart.data.labels= ['HK Island', 'Kowloon', 'ST TP', 'TW KT'];

// 	myLineChart.data.datasets[0].data= [arr[0],arr[1],arr[2],arr[3]];
// //   myLineChart.data.labels[7] = "Newly Added";
//   myLineChart.update();
// }



// var option = {
// 	showLines: true
// };
// var myLineChart = Chart.Line(canvas,{
//     data:data,
//   options:option,

// });
















































/////////////////////////////////////////////////////////////////
// document.querySelector('#district111')
//         .addEventListener('click', async function() {
//             let ctx = document.getElementById('myChart1');

//             let myChart = new Chart(ctx, {
//                 type: 'pie',
//                 data: {
//                     labels: ['HK Island', 'Kowloon', 'ST TP', 'TW KT'],
//                     datasets: [{
//                         label: 'Number of posts',
//                         data: [arr[0],arr[1],arr[2],arr[3]],
//                         backgroundColor: [
//                             'rgba(255, 99, 132, 0.2)',
//                             'rgba(54, 162, 235, 0.2)',
//                             'rgba(255, 206, 86, 0.2)',
//                             'rgba(75, 192, 192, 0.2)',
//                             // 'rgba(153, 102, 255, 0.2)',
//                             // 'rgba(255, 159, 64, 0.2)'
//                         ],
//                         borderColor: [
//                             'rgba(255, 99, 132, 1)',
//                             'rgba(54, 162, 235, 1)',
//                             'rgba(255, 206, 86, 1)',
//                             'rgba(75, 192, 192, 1)',
//                             // 'rgba(153, 102, 255, 1)',
//                             // 'rgba(255, 159, 64, 1)'
//                         ],
//                         borderWidth: 5 
//                     }]
//                 },
//                 options: {
//                     scales: {
//                         yAxes: [{
//                             ticks: {
//                                 beginAtZero: true
//                             }
//                         }]
//                     }
//                 }
//             });
//         })

// document.querySelector('#salary')
//         .addEventListener('click', async function() {
//             let ctx = document.getElementById('myChart2');

//             let myChart = new Chart(ctx, {
//                 type: 'pie',
//                 data: {
//                     labels: ['< 20k', '$20k - $29k', '$30k - $40k'],
//                     datasets: [{
//                         label: 'Number of posts',
//                         data: [arrSalary[0],arrSalary[1],arrSalary[2]],
//                         backgroundColor: [
//                             'rgba(255, 99, 132, 0.2)',
//                             'rgba(54, 162, 235, 0.2)',
//                             'rgba(255, 206, 86, 0.2)',
//                             // 'rgba(75, 192, 192, 0.2)',
//                             // 'rgba(153, 102, 255, 0.2)',
//                             // 'rgba(255, 159, 64, 0.2)'
//                         ],
//                         borderColor: [
//                             'rgba(255, 99, 132, 1)',
//                             'rgba(54, 162, 235, 1)',
//                             'rgba(255, 206, 86, 1)',
//                             // 'rgba(75, 192, 192, 1)',
//                             // 'rgba(153, 102, 255, 1)',
//                             // 'rgba(255, 159, 64, 1)'
//                         ],
//                         borderWidth: 5 
//                     }]
//                 },
//                 options: {
//                     scales: {
//                         yAxes: [{
//                             ticks: {
//                                 beginAtZero: true
//                             }
//                         }]
//                     }
//                 }
//             });
//         })

// document.querySelector('#source')
//         .addEventListener('click', async function() {
//             let ctx = document.getElementById('myChart3');

//             let myChart = new Chart(ctx, {
//                 type: 'pie',
//                 data: {
//                     labels: ['jobMarket', 'jobsDB'],
//                     datasets: [{
//                         label: 'Number of posts',
//                         data: [arrSource[0],arrSource[1]],
//                         backgroundColor: [
//                             'rgba(255, 99, 132, 0.2)',
//                             'rgba(54, 162, 235, 0.2)',
//                             // 'rgba(255, 206, 86, 0.2)',
//                             // 'rgba(75, 192, 192, 0.2)',
//                             // 'rgba(153, 102, 255, 0.2)',
//                             // 'rgba(255, 159, 64, 0.2)'
//                         ],
//                         borderColor: [
//                             'rgba(255, 99, 132, 1)',
//                             'rgba(54, 162, 235, 1)',
//                             // 'rgba(255, 206, 86, 1)',
//                             // 'rgba(75, 192, 192, 1)',
//                             // 'rgba(153, 102, 255, 1)',
//                             // 'rgba(255, 159, 64, 1)'
//                         ],
//                         borderWidth: 5 
//                     }]
//                 },
//                 options: {
//                     scales: {
//                         yAxes: [{
//                             ticks: {
//                                 beginAtZero: true
//                             }
//                         }]
//                     }
//                 }
//             });
//         })

