document.querySelector('#select-filter').addEventListener('click', function () {
    document.querySelector('#filter-search').classList.remove('disabledbutton');
    document.querySelector('#filter-search-selections').classList.remove('disabledbutton');
    document.querySelector('#keyword-search').classList.add('disabledbutton');
})

document.querySelector('#select-search').addEventListener('click', function () {
    document.querySelector('#keyword-search').classList.remove('disabledbutton');
    document.querySelector('#filter-search').classList.add('disabledbutton');
    document.querySelector('#filter-search-selections').classList.add('disabledbutton');
})

// function keywordSearch() {
//     let keywordInput = [];
//     let keywordValue = document.getElementById("keyword-input").value;
//     keywordInput.push(keywordValue);
//     console.log(keywordInput);
// }

document.getElementById('aaa').addEventListener("submit", async function (event) {
    event.preventDefault();

    let allPicks = []
    // let jobOptions = document.getElementById("pick-jobs");
    let locationOptions = document.getElementById("pick-locations");
    let salaryOptions = document.getElementById("pick-salary");
    let sourceOptions = document.getElementById("pick-source");

    // for (let option of jobOptions.options) {
    //     if (option.selected) {
    //         allPicks.push(option.value);
    //     }
    // }

    for (let option of locationOptions.options) {
        if (option.selected) {
            allPicks.push(option.value);
        }
    }

    for (let option of salaryOptions.options) {
        if (option.selected) {
            allPicks.push(option.value);
        }
    }

    for (let option of sourceOptions.options) {
        if (option.selected) {
            allPicks.push(option.value);
        }
    }
    // const form = even.target;
    // const formObject = {
    //     content: form.content.value
    // }

    // const res = await fetch('/filter', {
    //     method: "POST",
    //     headers:{
    //         "Content-Type": "application/json"
    //     },
    //     body: JSON.stringify(allPicks)
    location.href = `/search-result.html?district=${allPicks[0]}&salary=${allPicks[1]}&source=${allPicks[2]}`
    // location.href = `/search}`
    // });
    // console.log(allPicks);
    // const result = await res.json();
});
//keyword search
document.getElementById('aaa-keywordSearch').addEventListener("submit", async function (event) {
    event.preventDefault();

    let allPicks = []
    let keywords = document.getElementById("keyword-input").value;
    console.log(keywords)
    
    location.href = `/search-result.html?q=${keywords}`
})
