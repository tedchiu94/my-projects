//const io = require('socket.io')();
// const socket = io('http://tedckh.xyz:1025')
const socket = io('https://tedsproject.xyz:443')
const messageForm = document.getElementById('send-container')
const messageInput = document.getElementById('message-input')
const messageContainer = document.querySelector('.mess-r')
const renderName = document.querySelector('div > h2')

const name = prompt('What\'s yr name?')
appendMessage('You joined')
socket.emit('new-user', name)
renderName.innerText = name

socket.on('user-connected', name => {
    appendMessage(`${name} has connected.`)
})

socket.on('user-disconnected', name => {
    appendMessage(`${name} has disconnected.`)
})

socket.on('chat-message2', data => {
    appendMessage(`${data.name} : ${data.message}`)
})

messageForm.addEventListener('submit', e => {
    e.preventDefault()
    const message = messageInput.value
    socket.emit('send-chat-message', message)
    messageInput.value = ''
    appendMessage(`You : ${message}`)
})

//Enter key as submition
messageForm.addEventListener('keyup', e => {
    e.preventDefault()
    if (e.keyCode === 13) {
        const message = messageInput.value
        socket.emit('send-chat-message', message)
        messageInput.value = ''
        appendMessage(`You : ${message}`)
    }
})

function appendMessage(message) {
    const messageElement = document.createElement('div')
    messageElement.innerText = message
    messageContainer.append(messageElement)
}