document.querySelector('#select-filter').addEventListener('click', function () {
    document.querySelector('#filter-search').classList.remove('disabledbutton');
    document.querySelector('#filter-search-selections').classList.remove('disabledbutton');
    document.querySelector('#keyword-search').classList.add('disabledbutton');
})

document.querySelector('#select-search').addEventListener('click', function () {
    document.querySelector('#keyword-search').classList.remove('disabledbutton');
    document.querySelector('#filter-search').classList.add('disabledbutton');
    document.querySelector('#filter-search-selections').classList.add('disabledbutton');
})

const params = new URLSearchParams(location.search)
const districtPicked = params.get('district');
const salaryPicked = params.get('salary');
const sourcePicked = params.get('source');
const keyword = params.get('q');

async function loadJobResults() {
    const res = await fetch('/search?district=' + districtPicked +'&salary='+salaryPicked+ '&source=' +sourcePicked);
    const postResults = await res.json();
    console.log(postResults)

    const postContainer = document.querySelector('#post-container');
    postContainer.innerHTML = "";

    for (let postResult of postResults) {
        postContainer.innerHTML += `<div class="col-sm-8 posting-summaries">
                <div class="posting-summary">
                    <div class="posting-logo-holder">  
                        <img class="posting-logo" src='images/${sourcePicked}.jpg'>
                    </div>
                    <hr>
                    <div>
                        <h5>Job Position: </h5>
                        ${postResult.job_position}
                    </div>
                    <div>
                        <h5>Company: </h5>
                        ${postResult.company}
                    </div>
                    <div>
                        <h5>District:</h5>
                        ${postResult.district}
                    </div>
                    <hr>
                    <div>
                        <a class="btn btn-light" href=${postResult.href} role="button" target="_blank">
                            Job Details
                        </a>
                    </div>
                    <ul>
                        
                    </ul>
                </div>
            </div>
        `
    }
}
    
async function keywordLoadJobResults() {
    const res = await fetch('/keywordSearch?q=' +keyword);
    const postResults = await res.json();
    console.log(postResults)

    const postContainer = document.querySelector('#post-container');
    postContainer.innerHTML = "";

    for (let postResult of postResults) {
        postContainer.innerHTML += `<div class="col-sm-8 posting-summaries">
                <div class="posting-summary">
                    <div class="posting-logo-holder">  
                        <img class="posting-logo" src='images/${postResult.resource_from}.jpg'>
                    </div>
                    <hr>
                    <div>
                        <h5>Job Position: </h5>
                        ${postResult.job_position}
                    </div>
                    <div>
                        <h5>Company: </h5>
                        ${postResult.company}
                    </div>
                    <div>
                        <h5>District:</h5>
                        ${postResult.district}
                    </div>
                    <hr>
                    <div>
                        <a class="btn btn-light" href=${postResult.href} role="button" target="_blank">
                            Job Details
                        </a>
                    </div>
                    <ul>
                        
                    </ul>
                </div>
            </div>
        `
    }
}

if (districtPicked || salaryPicked || sourcePicked) {
    loadJobResults();
}

if (keyword) {
    keywordLoadJobResults();
}
