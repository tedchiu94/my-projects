async function loadNewPosts() {
    const res = await fetch('/newPosts');
    const postResults = await res.json();

    const postContainer = document.querySelector('#new-post-container');
    postContainer.innerHTML = "";
     
    for (let index in postResults) {
        if (index < 4) {
            console.log(postResults[index].resource_from);
            postContainer.innerHTML += `<div class="col-sm-3">
                    <div class="new-post-box">
                        <div class="new-post-heading">
                            <img src='images/${postResults[index].resource_from}.jpg' class="new-post-logo img-fluid">
                            <div class="new-post-content">${postResults[index].job_position}</div>
                        </div>
                        <div class="new-post-contents">
                            <div class="new-post-content">Company:</div>
                            <div class="new-post-content">${postResults[index].company}</div>
                            <div class="new-post-content">Location:</div>
                            <div class="new-post-content">${postResults[index].district}</div>
                            <a class="btn btn-light link" href=${postResults[index].href} role="button" target="_blank">LINK</a>
                        </div>
                    </div>
                </div>
            `
        }
        else {
            return
        }
    }
}

loadNewPosts()