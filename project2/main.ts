import express,{ Response, Request } from 'express';
import expressSession from 'express-session';
import path from 'path';
import socketIO from 'socket.io';
import http from 'http';
import dotenv from 'dotenv';

import Knex from 'knex';
const knexConfig = require('./knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"])


dotenv.config();

// connect DB
// export const client = new pg.Client({
//     database:process.env.DB_NAME,
//     user:process.env.DB_USERNAME,
//     password:process.env.DB_PASSWORD,
//     host:"localhost",
//     port:5432
// });
// client.connect();

//connent express and socket.io
const app = express();
const server = new http.Server(app);
const io = socketIO(server);

app.use(expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave:true,
    saveUninitialized:true
}));

app.post('/', (req:Request,res:Response)=>{
    res.redirect('/');
});

// Recommend post on index page
// Serach post on Serach result page
import {PostService,RecommendPostService} from './services/postService';
import {PostController,RecommendPostsController} from './controllers/postController';

const postService = new PostService(knex);
export const postController = new PostController(postService);
const recommendPostService = new RecommendPostService(knex);
export const recommendPostController = new RecommendPostsController(recommendPostService);

// Chart result on Chartjs
import {ChartService} from './services/chartService';
import {ChartController} from './controllers/chartController';
const chartService = new ChartService(knex);
export const chartController = new ChartController(chartService);

import {routes} from './routes';
app.use('/',routes);

app.get('/keywordSearch', async (req, res) => {
    const rows = await knex.raw(/*sql*/`
        SELECT post.* from post
        WHERE post.job_position ILIKE ?
        OR post.company ILIKE ?
        OR post.district ILIKE ? ;
    `, ['%'+req.query.q+'%', '%'+req.query.q+'%', '%'+req.query.q+'%'])
    res.json(rows.rows)
})

app.use(express.static('public'));
app.use((req,res)=>{
    res.sendFile(path.join(__dirname,'public/404.html'))
});

const users = {};

io.on('connection', socket => {
    // console.log('new user!')
    socket.on('new-user', name => {
        // console.log(socket.id)
        users[socket.id] = name
        console.log(users)
        socket.broadcast.emit('user-connected', name)
    })
        
    socket.on("send-chat-message", message => {
        // console.log(message)
        socket.broadcast.emit('chat-message2', {message: message,
            name: users[socket.id]})
        })
    
        socket.on('disconnect', () => {
            socket.broadcast.emit('user-disconnected', users[socket.id])
            delete users[socket.id]
    })
}) 

const PORT = 8080;
server.listen(PORT, ()=>{
    console.log(`Listening at http://localhost${PORT}/`);
});