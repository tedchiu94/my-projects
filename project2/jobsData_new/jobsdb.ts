import puppeteer from 'puppeteer';
// import jsonfile from 'jsonfile';



// const puppeteer = require('puppeteer');
// const jsonfile = require("jsonfile");

(async () => {

    const browser = await puppeteer.launch({ headless: false });
    const page = await browser.newPage();
    await page.goto('https://hk.jobsdb.com/hk/jobs/information-technology/full-time-employment/1?Key=web%20developer&Locations=153%2C166%2C175%2C150&SalaryF=0&SalaryT=2147483647&SalaryType=1', { waitUntil: "networkidle2" });

    // search criteria: to select location 
    // let location_btn = await page.waitForSelector('#contentContainer > div.sticky-outer-wrapper._2Sz6Z_1my > div > div > div > div > div.FYwKg._1GAuD._2II51_1my.HdpOi > div > div > div > div.FYwKg._20Cd9.BIdp3._3Ve9Z > div > span > span');
    // await location_btn.click();

    // let allLocation_btn = await page.waitForSelector('#contentContainer > div.sticky-outer-wrapper._2Sz6Z_1my > div > div > div > div > div.FYwKg._1GAuD._2II51_1my.HdpOi > form > div:nth-child(2) > div > div > div > div:nth-child(3) > div > svg');
    // await allLocation_btn.click();


    // let selectedLocation_btn = await page.waitForSelector('#contentContainer > div.sticky-outer-wrapper._2Sz6Z_1my > div > div > div > div > div.FYwKg._1GAuD._2II51_1my.HdpOi > form > div:nth-child(2) > div > div:nth-child(2) > div > div > div.FYwKg._3CF2g_1my > div > div > div:nth-child(1) > label > div');
    // await selectedLocation_btn.click();

    // let selectedLocation2_btn = await page.waitForSelector('#contentContainer > div.sticky-outer-wrapper._2Sz6Z_1my > div > div > div > div > div.FYwKg._1GAuD._2II51_1my.HdpOi > form > div:nth-child(2) > div > div:nth-child(2) > div > div > div.FYwKg._3CF2g_1my > div > div > div:nth-child(3) > label > div');
    // await selectedLocation2_btn.click(); 

    // let viewJobs_btn = await page.waitForSelector('#contentContainer > div.sticky-outer-wrapper._2Sz6Z_1my > div > div > div > div > div.FYwKg._1GAuD._2II51_1my.HdpOi > form > div:nth-child(2) > div > div:nth-child(2) > div > div > div.FYwKg.Sr3lG_1my > div > button');
    // await viewJobs_btn.click();

    // await page.waitForNavigation({
    //     waitUntil: 'networkidle0',
    // });

    
    // //search criteria: to select salary range
    let salary_btn = await page.$('#contentContainer > div.sticky-outer-wrapper._2Sz6Z_1my > div > div > div > div > div.FYwKg.rtHBp_1my > div > div > div > div:nth-child(1) > div > div > div:nth-child(1) > div > span > button');
    if (salary_btn) {
        await salary_btn.click();
    }

    let maxSalaryBtn = await page.waitForSelector('#rangeContainer > div.rc-slider.xQqME_1my > div:nth-child(5) > div.FYwKg._2YogD._fzNe_1my._3qA9G_1my')
    const maxInfo = await maxSalaryBtn.boundingBox();
    // console.log(maxInfo);

    let minSalaryBtn = await page.waitForSelector('#rangeContainer > div.rc-slider.xQqME_1my > div:nth-child(4) > div.FYwKg._2YogD._fzNe_1my._3jbF9_1my')
    const minInfo = await minSalaryBtn.boundingBox();
    // console.log(minInfo);

    // to select salary ranged $0-$20k
    // await page.mouse.move(maxInfo.x, maxInfo.y);
    // await page.mouse.down();

    // let oneInterval = (maxInfo.x - minInfo.x)/10;

    // for (let i = 0; i < oneInterval*7; i=i+oneInterval){
    //     await page.mouse.move(maxInfo.x-i, maxInfo.y);
    // }

    // await page.mouse.up();

    // to select salary ranged $20k-$30k
    if (maxInfo && minInfo) {
        await page.mouse.move(maxInfo.x, maxInfo.y);
        await page.mouse.down();
    
        let oneInterval = (maxInfo.x - minInfo.x) / 10;
    
        for (let i = 0; i <= oneInterval * 5; i = i + oneInterval) {
            await page.mouse.move(maxInfo.x - i, maxInfo.y);
        }
    
        await page.mouse.up();
        
       
        await page.mouse.move(minInfo.x, minInfo.y);
        await page.mouse.down();
    
        for (let i = 0; i < oneInterval * 5; i = i + oneInterval) {
            await page.mouse.move(minInfo.x + i, minInfo.y);
        }
    
        await page.mouse.up();
    }

    //Apply filter for salary range selection
    let filter_btn = await page.$('#contentContainer > div.sticky-outer-wrapper._2Sz6Z_1my > div > div > div > div > div.FYwKg.rtHBp_1my > div > div > div > div:nth-child(1) > div > div > div.FYwKg._3ERpG_1my._2_vLJ._38Fxk_1my._1lay8_1my > div > form > div > div:nth-child(2) > div.FYwKg.af1kF_1my._20Cd9.Im1ty.zoxBO_1my._3qNSL_1my._3RqUb_1my._1YKq0_1my.LH6N5_1my > button');
    if (filter_btn) {
        await filter_btn.click();
    }

    await page.waitForNavigation({
        waitUntil: 'networkidle0',
    });


    //1st page
    //To list out job openings 
    let str = `div[data-search-sol-meta] > div`;
    await page.waitFor(str, { visible: true });
    await page.waitForSelector(str);
    // const lists = await page.$$eval(str, eles => eles.map(ele => ele.innerText));         

    // console.log(lists);


    //To obtain href of job results
    // const hrefs = await page.evaluate(
    //     () => Array.from(
    //       document.querySelectorAll('div > h1 > a'),
    //       a => a.getAttribute('href')
    //     )
    //   );

    const itJob = [];
    // for(let index in lists){
    //         const postDataArray = lists[index].split('\n');
    //         itJob.unshift({
    //             job_position: postDataArray[0],
    //             company:postDataArray[1],
    //             district: `HK Island`,
    //             // salary : `< $20k`,
    //             // salary : `$20k - $30k`,
    //             salary : `$30k - $40k`,
    //             href : hrefs[index],
    //             resource_from: `jobsDB`
    //         });
    // };   

    //To go to next page
    // let next_btn = await page.$('#contentContainer > div.FYwKg._3VCZm > div > div.FYwKg._2aTwK_1my > div.FYwKg._20Cd9._36UVG_1my > div > a');
    // await next_btn.click();

    // //2nd page
    // await page.waitFor(str, { visible: true });
    // await page.waitForSelector(str);
    // const lists2 = await page.$$eval(str, eles => eles.map(ele => ele.innerText));         


    // //To obtain href of job results
    // const hrefs2 = await page.evaluate(
    //     () => Array.from(
    //       document.querySelectorAll('div > h1 > a'),
    //       a => a.getAttribute('href')
    //     )
    //   );


    // for(let index in lists2){
    //         const postDataArray = lists2[index].split('\n');
    //         itJob.unshift({
    //             job_position: postDataArray[0],
    //             company:postDataArray[1],
    //             district: `HK Island`,
    //             // salary : `< $20k`,
    //             // salary : `$20k - $30k`,
    //             salary : `$30k - $40k`,
    //             href : hrefs2[index],
    //             resource_from: `jobsDB`
    //         });
    // };

    //To go to 3rd page
    // await page.waitFor('#contentContainer > div.FYwKg._3VCZm > div > div.FYwKg._2aTwK_1my > div.FYwKg._20Cd9._36UVG_1my > div > a:nth-child(3)', { visible: true });
    // let next3_btn = await page.$('#contentContainer > div.FYwKg._3VCZm > div > div.FYwKg._2aTwK_1my > div.FYwKg._20Cd9._36UVG_1my > div > a:nth-child(3)');
    // await next3_btn.click();

    console.log(itJob.length);

    // await jsonfile.writeFile('./jobsData/jobsDb/hkIsland_30k-40k.json', itJob,{spaces:4});
   
})();




// (async () => {

//     const browser = await puppeteer.launch({headless : true, 
//         args:[
//             '--no-sandbox',
//             '--disable-setuid-sandbox'
//         ]});
//     const page = await browser.newPage();
//     await page.goto('https://hk.jobsdb.com/hk/jobs/information-technology/1', {waitUntil: "networkidle2"});
    
//     let listTotal = [];
//     for (let i = 1; i < 20; i++) {
//         let str = '#contentContainer > div.FYwKg._3VCZm > div > div.FYwKg._2aTwK_1my > div:nth-child(3) > div > div:nth-child('+i+') > div';
//         // console.log(str);
//         await page.waitFor(str, {visible:true});
//         await page.waitForSelector(str);
//         const list = await page.$$eval(str,eles => eles.map(ele => ele.innerText));
//         listTotal.push(list);
//     }

    
//     console.log(listTotal);
// })();