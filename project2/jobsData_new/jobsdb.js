const puppeteer = require('puppeteer');
const jsonfile = require("jsonfile");

(async () => {

    const browser = await puppeteer.launch({ headless: false });
    const page = await browser.newPage();
    await page.goto('https://hk.jobsdb.com/hk/jobs/information-technology/full-time-employment/1?Key=web%20developer', { waitUntil: "networkidle2" });
    

    //search criteria: to select salary range
    // let salary_btn = await page.$('#contentContainer > div.sticky-outer-wrapper._2Sz6Z_1my > div > div > div > div > div.FYwKg.rtHBp_1my > div > div > div > div:nth-child(1) > div > div > div:nth-child(1) > div > span > button');
    // await salary_btn.click();

    // let maxSalaryBtn = await page.waitForSelector('#rangeContainer > div.rc-slider.xQqME_1my > div:nth-child(5) > div.FYwKg._2YogD._fzNe_1my._3qA9G_1my')
    // const maxInfo = await maxSalaryBtn.boundingBox();
    // // console.log(maxInfo);

    // let minSalaryBtn = await page.waitForSelector('#rangeContainer > div.rc-slider.xQqME_1my > div:nth-child(4) > div.FYwKg._2YogD._fzNe_1my._3jbF9_1my')
    // const minInfo = await minSalaryBtn.boundingBox();
    // // console.log(minInfo);

    // // // to select salary ranged $0-$20k
    // await page.mouse.move(maxInfo.x, maxInfo.y);
    // await page.mouse.down();

    // let oneInterval = (maxInfo.x - minInfo.x)/10;

    // for (let i = 0; i < oneInterval*7; i=i+oneInterval){
    //     await page.mouse.move(maxInfo.x-i, maxInfo.y);
    // }

    // await page.mouse.up();

    // // to select salary ranged $20k-$30k
    // await page.mouse.move(maxInfo.x, maxInfo.y);
    // await page.mouse.down();

    // let oneInterval = (maxInfo.x - minInfo.x) / 10;

    // for (let i = 0; i <= oneInterval * 7; i = i + oneInterval) {
    //     await page.mouse.move(maxInfo.x - i, maxInfo.y);
    // }

    // await page.mouse.up();

    // await page.mouse.move(minInfo.x, minInfo.y);
    // await page.mouse.down();

    // for (let i = 0; i < oneInterval * 4; i = i + oneInterval) {
    //     await page.mouse.move(minInfo.x + i, minInfo.y);
    // }

    // await page.mouse.up();


    //Apply filter for salary range selection
    // let filter_btn = await page.$('#contentContainer > div.sticky-outer-wrapper._2Sz6Z_1my > div > div > div > div > div.FYwKg.rtHBp_1my > div > div > div > div:nth-child(1) > div > div > div.FYwKg._3ERpG_1my._2_vLJ._38Fxk_1my._1lay8_1my > div > form > div > div:nth-child(2) > div.FYwKg.af1kF_1my._20Cd9.Im1ty.zoxBO_1my._3qNSL_1my._3RqUb_1my._1YKq0_1my.LH6N5_1my > button');
    // await filter_btn.click();

    // await page.waitForNavigation({
    //     waitUntil: 'networkidle0',
    // });

    //search criteria: to select location
    // let location_btn = await page.waitForSelector('#contentContainer > div.sticky-outer-wrapper._2Sz6Z_1my > div > div > div > div > div.FYwKg._1GAuD._2II51_1my.HdpOi > div > div > div > div.FYwKg._20Cd9.BIdp3._3Ve9Z > div > span > span');
    // await location_btn.click();

    // let allLocation_btn = await page.waitForSelector('#contentContainer > div.sticky-outer-wrapper._2Sz6Z_1my > div > div > div > div > div.FYwKg._1GAuD._2II51_1my.HdpOi > form > div:nth-child(2) > div > div > div > div:nth-child(3) > div > svg');
    // await allLocation_btn.click();


    // let selectedLocation_btn = await page.waitForSelector('#contentContainer > div.sticky-outer-wrapper._2Sz6Z_1my > div > div > div > div > div.FYwKg._1GAuD._2II51_1my.HdpOi > form > div:nth-child(2) > div > div:nth-child(2) > div > div > div.FYwKg._3CF2g_1my > div > div > div:nth-child(1) > label > div');
    // await selectedLocation_btn.click();

    // let viewJobs_btn = await page.waitForSelector('#contentContainer > div.sticky-outer-wrapper._2Sz6Z_1my > div > div > div > div > div.FYwKg._1GAuD._2II51_1my.HdpOi > form > div:nth-child(2) > div > div:nth-child(2) > div > div > div.FYwKg.Sr3lG_1my > div > button');
    // await viewJobs_btn.click();

    // await page.waitForNavigation({
    //     waitUntil: 'networkidle0',
    // });

    //search criteria: to select employment-type
    // let employType_btn = await page.$('#contentContainer > div.sticky-outer-wrapper._2Sz6Z_1my > div > div > div > div > div.FYwKg.rtHBp_1my > div > div > div > div:nth-child(1) > div > div > div:nth-child(2) > div > span > button');
    // await employType_btn.click();

    // await page.waitForNavigation({
    //     waitUntil: 'networkidle0',
    // });

    // let selectEmployType_btn = await page.$('#contentContainer > div.sticky-outer-wrapper._2Sz6Z_1my > div > div > div > div > div.FYwKg.rtHBp_1my > div > div > div > div:nth-child(1) > div > div > div.FYwKg._3ERpG_1my._2_vLJ._38Fxk_1my._1lay8_1my > div > form > div > div:nth-child(1) > div > div > div:nth-child(2) > div > div > div > div:nth-child(1) > label > div');
    // await selectedLocation_btn.click();



    //To list out job openings 
        let str = `div[data-search-sol-meta] > div`
        await page.waitFor(str, { visible: true });
        await page.waitForSelector(str);
        const lists = await page.$$eval(str, eles => eles.map(ele => ele.innerText));         

        // for(let list of lists){
            


        //     // await client.query(`INSERT INTO "post" (resource_from,job_position,company,district) values($1,$2,$3,$4)`,["jobsdb",postDataArray[0],postDataArray[1],postDataArray[2]]);
        // }
        const postDataArray = lists[0].split('\n');            
        console.log(postDataArray[0])
        console.log(postDataArray[1])
        console.log(postDataArray[2])
    
    // console.log(list);

   
    // const jsonStrListTotal = JSON.stringify(list);
    // await jsonfile.writeFile('./jobsdb.json', jsonStrListTotal, {spaces:4})

   
    
})();

