const puppeteer = require('puppeteer');
const jsonfile = require("jsonfile");

(async () => {

    const browser = await puppeteer.launch({ headless: false });
    const page = await browser.newPage();
    await page.goto('https://www.jobmarket.com.hk/Job/?Searchtext=programmer&AreaID=2&JobType=1&SalaryTo=40000&SalaryFrom=31000&SalaryTypeID=1&IsDesc=1', { waitUntil: "networkidle2" });
   
    
    //To switch to iframe
    const frame = await page.frames().find(frame => frame.url().includes('https://www.jobmarket.com.hk/Job/JobList?'));

    //To list out job openings 
    let str = `ul > li`;
    await frame.waitFor(str, { visible: true });
    await frame.waitForSelector(str);
    const lists = await frame.$$eval(str, eles => eles.map(ele => ele.innerText));

    console.log(lists.length);

    //To obtain href of job results
    const hrefs = await frame.evaluate(
    () => Array.from(
        document.querySelectorAll('ul > li'),
        a => a.getAttribute('data-jobid')
    )
    );
    
    let fullhrefs = []
    for (let href of hrefs) {
        const fullhref = 'https://www.jobmarket.com.hk/Job/CurrentJob?Jobid='.concat('',href);
        fullhrefs.push(fullhref);
    }

    console.log(fullhrefs.length);
    console.log(fullhrefs);

    const itJob = [];
    for(let index in lists){
            const postDataArray = lists[index].split('\n');
            itJob.unshift({
                job_position: postDataArray[0],
                company:postDataArray[2],
                district: `Kowloon`,
                // salary : `< $20k`,
                // salary : `$21k - $30k`,
                salary : `$31k - $40k`,
                href : fullhrefs[index],
                resource_from: `jobMarket`
            });
    };   
    await jsonfile.writeFile('./jobsData/jobMarket/Kowloon_31k-40k.json', itJob,{spaces:4});


})();

