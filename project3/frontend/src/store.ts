import { createStore, combineReducers, applyMiddleware, compose } from "redux"
import { QuestionState, questionReducer } from "./redux/AllQuestions/reducer"
import { QuestionActions } from "./redux/AllQuestions/actions"
import { RouterState, RouterAction, connectRouter, routerMiddleware } from "connected-react-router"
import { createBrowserHistory} from 'history'
import thunk,{ThunkDispatch as OldThunkDispatch} from 'redux-thunk'
import { AuthState, authReducer } from "./auth/reducer"
import { AuthActions } from "./auth/action"
import { AskQState, askQReducer } from "./redux/AskQuestion/reducer"
import { AskQActions } from "./redux/AskQuestion/actions"
import { ReadQState, readQReducer } from "./redux/TheQuestion/reducer"
import { ReadQActions } from "./redux/TheQuestion/actions"
import { CheckedAction } from "./redux/CheckBoxFavorite/action"
import { checkBoxReducer } from "./redux/CheckBoxFavorite/reducer"
import { SearchedQuestionAction } from "./redux/SearchedQuestions/action"
import { searchedQuestionReducer, SearchedQuestionState } from "./redux/SearchedQuestions/reducer"
import { SearchTextAction } from "./redux/SearchText/action"
import { searchTextReducer } from "./redux/SearchText/reducer"
import { RepliesQuestionState } from "./redux/AllTheReplies/reducer"
import { ReplyActions } from "./redux/AllTheReplies/actions"
import {replyReducer} from './redux/AllTheReplies/reducer' 




export const history = createBrowserHistory()

export interface RootState {
    question: QuestionState
    askQ: AskQState
    readQ: ReadQState
    router: RouterState
    // Carl
    auth: AuthState
    searchQuestion: SearchedQuestionState
    searchText: string
    checkBox: boolean[]
    // ted
    repliesQuestion: RepliesQuestionState
    
}

export type RootActions = RouterAction | QuestionActions  | AskQActions | ReadQActions | AuthActions | CheckedAction |SearchedQuestionAction |SearchTextAction |ReplyActions 

const reducers = combineReducers({
    question: questionReducer,
    askQ: askQReducer,
    readQ: readQReducer, 

    //question
    router: connectRouter(history),
    // Carl
    auth: authReducer,
    checkBox: checkBoxReducer,
    searchQuestion: searchedQuestionReducer,
    searchText: searchTextReducer,
    // Carl
    repliesQuestion: replyReducer
})


declare global{
    /* tslint:disable:interface-name */
    interface Window{
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__:any
    }
}

export type ThunkDispatch = OldThunkDispatch<RootState, null, RootActions>

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore<RootState, RootActions, {}, {}>(reducers, composeEnhancers(applyMiddleware(thunk), applyMiddleware(routerMiddleware(history))))
