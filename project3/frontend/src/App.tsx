import React, { useEffect } from 'react'
import './App.scss'
import Login from './pages/Login'
import { useDispatch, useSelector } from 'react-redux'
import { Route, Switch } from 'react-router-dom'
import HomePage from './pages/HomePage'
import AllTheQuestions from './pages/AllTheQuestions'
import CreateAccount from './pages/CreateAccount'
import { RootState } from './store'
import { restoreLogin } from './auth/thunk'
import { push } from 'connected-react-router'
import ProfilePage from './pages/ProfilePage'
import UserRecordPage from './pages/UserRecordQuestionsPage'

function App() {
  // Carl
  const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(restoreLogin())
  }, [dispatch])
  // Carl
  return (
    <>
      <Switch>
        {/* Carl */}
        <Route path="/login">
          <div>
            {isAuthenticated && <>
              {dispatch(push(`/`))}
            </>}
            {!isAuthenticated && <Login />}
          </div>
        </Route>
        <Route path="/CreateAccount"><CreateAccount /></Route>
        {/* Carl */}
        <Route path="/" exact><HomePage /></Route>
        <Route path="/question/ask"><HomePage /></Route>
        <Route path="/question/all"><AllTheQuestions /></Route>
        <Route path="/question/:id"><HomePage /></Route>
        <Route path="/Profile"><ProfilePage /></Route>
        <Route path="/UserRecord"><UserRecordPage /></Route>
        <Route path="/question/most_views"><HomePage /></Route>
        <Route path="/question/hottest"><HomePage /></Route>
        <Route path="/question/most_likes"><HomePage /></Route>
        <Route path="/question/answered"><HomePage /></Route>
        <Route path="/question/open"><HomePage /></Route>
      </Switch>
    </>
  );
}
export default App;
