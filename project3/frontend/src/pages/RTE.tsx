import React, { useState } from 'react';
import RichTextEditor from 'react-rte';


export default function BodyTextEditor() {

  const [editorValue, setEditorValue] =
    useState(RichTextEditor.createEmptyValue());
  //@ts-ignore
  const handleChange = value => {
    setEditorValue(value);

  };

  return (
    <RichTextEditor
      value={editorValue}
      onChange={handleChange}
    />
  );
}

