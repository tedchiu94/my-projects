import React, { useEffect, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../store'
import { fetchQuestions, Question } from '../redux/AllQuestions/actions'
import { closeReadQ } from '../redux/TheQuestion/actions'
import { push } from 'connected-react-router'
import ReplyQuestion from './ReplyQuestion'
import AllTheReplies from './AllTheReplies'
import { loadedReplies } from '../redux/AllTheReplies/actions'
import ReactMarkdown from 'react-markdown'
import whiteHeart from '../images/whiteheart.png'
import redHeart from '../images/redheart.png'

function TheQuestion() {
    const dispatch = useDispatch()
    const questionID = useSelector((state: RootState) => state.readQ.questionID)
    const question: Question | undefined = useSelector((state: RootState) => state.question.questionById[questionID])
    const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated)
    const token = useSelector((state: RootState) => state.auth.token)

    useEffect(() => {
        dispatch(fetchQuestions())
    }, [dispatch])

    useEffect(() => {
        const timer = setTimeout(async () =>
            await fetch(`${process.env.REACT_APP_BACKEND_URL}/question/${questionID}/addviews`, {
                method: "PUT",
            })
            , 1000 * 2)

        return () => clearTimeout(timer)
    }, [questionID])

    // ted fetch replies
    const getReplies = useCallback(async function(i: number) {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/replies`);
        const json = await res.json();
        dispatch(loadedReplies(json[i]))
    }, [dispatch])

    useEffect(() => {
        getReplies(questionID);
    }, [getReplies, questionID])

    return (
        <div className="AskQHolder">
            <div className="AskQuestion">
                <button className="CloseButton" onClick={() => {
                    dispatch(closeReadQ())
                    dispatch(push('/'))
                }}>X</button>
                <div>
                    <h3>Question ID: {questionID}</h3>
                    <h3>Question Topic: {question?.question_topic} </h3>
                    <h3>Question Tag: {question?.question_tag}</h3>
                </div>
                <div className="TheQuestionContent">
                    <h3 className="Details">Question Details:
                    {isAuthenticated && question.liked === "0" &&
                            <div className="Feature-click">
                                <img className="heartIcon" src={whiteHeart} alt="heartIcon"
                                    onClick={async () => {
                                        await fetch(`${process.env.REACT_APP_BACKEND_URL}/question/${question.id}/like`, {
                                            method: 'PUT',
                                            headers: {
                                                Authorization: `Bearer ${token}`
                                            }
                                        })
                                        dispatch(fetchQuestions())
                                    }} />
                            </div>}
                        {isAuthenticated && question.liked === "1" &&
                            <div className="Feature-click">
                                <img className="heartIcon" src={redHeart} alt="heartIcon"
                                    onClick={async () => {
                                        await fetch(`${process.env.REACT_APP_BACKEND_URL}/question/${question.id}/unlike`, {
                                            method: 'PUT',
                                            headers: {
                                                Authorization: `Bearer ${token}`
                                            }
                                        })
                                        dispatch(fetchQuestions())
                                    }} />
                            </div>}
                    </h3>
                    <ReactMarkdown source={question?.question_content} />
                </div>
                <AllTheReplies />
                <ReplyQuestion />
                <button className="UserRecordButton" onClick={() => {
                    dispatch(closeReadQ())
                    dispatch(push('/profile'))
                }}>Profile</button>
            </div>
        </div>
    )
}

export default TheQuestion;
