import React, { useState } from 'react';
import { Question } from '../redux/AllQuestions/actions';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../store';
import { useFormState } from 'react-use-form-state';
import { loadedReplies } from '../redux/AllTheReplies/actions'

function ReplyQuestion() {

    const dispatch = useDispatch();
    const [formState, { textarea }] = useFormState();
    const questionID = useSelector((state: RootState) => state.readQ.questionID)
    const question: Question | undefined = useSelector((state: RootState) => state.question.questionById[questionID])
    const user = useSelector((state: RootState) => state.auth.user?.id)
    const [file, setFile] = useState<File | undefined>(undefined);

    async function getReplies (i:number){
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/replies`);
        const json = await res.json();
        dispatch(loadedReplies(json[i]))
    }

    return (
        <>
            <h1 className="AskQuestionHeader">Reply this question </h1>
            <form className="QuestionForm" onSubmit={async event => {
                event.preventDefault();
                const formData = new FormData();
                
                formData.append('reply_content', formState.values.reply_content)
                if (user) {
                    formData.append('userId', user + '')
                };
                if (question.id) {
                    formData.append('questionId', question.id + '')
                };
                if (file) {
                    formData.append('photo', file)
                };

                await fetch(`${process.env.REACT_APP_BACKEND_URL}/replies`, {
                    method: 'POST',
                    body: formData
                })
                await getReplies(questionID)
            }}>
                <label>
                    <textarea className="QuestionDetails" placeholder="Question details..." {...textarea('reply_content')} required />
                </label>
                <input type="file" onChange={event => setFile(event.currentTarget.files?.[0])} />               
                <input className="SubmitButton" type="submit" value="submit" />
            </form>
        </>
    )
}

export default ReplyQuestion;
