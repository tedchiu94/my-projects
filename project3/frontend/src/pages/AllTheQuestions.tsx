import React, { useEffect, useState, useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../store';
import { fetchMostViewsQs, fetchHottestQs, fetchMostLikesQs, fetchAnsweredQs, fetchUnansweredQs } from '../redux/AllQuestions/actions';
import { push } from 'connected-react-router';
import { checkedOrNot } from '../redux/CheckBoxFavorite/action';
import TotalQuestions from './TotalQuestions';
import { readQ } from '../redux/TheQuestion/actions';

function AllTheQuestions() {
  const dispatch = useDispatch()
  const [selectQPeriod, setSelectQPeriod] = useState('30')
  const questionByIDs = useSelector((state: RootState) => state.question.questionIds)
  const questions = useSelector((state: RootState) => questionByIDs.map(id => state.question.questionById[id]))
  const userID = useSelector((state: RootState) => state.auth.user?.id)
  const checkedBoxes = useSelector((state: RootState) => state.checkBox)
  const searchedQuestionsByIDs = useSelector((state: RootState) => state.searchQuestion.questionIds)
  const searchText = useSelector((state: RootState) => state.searchText);
  const searchedQuestions = useSelector((state: RootState) => searchedQuestionsByIDs.map(id => state.searchQuestion.questionById[id]))
  const [displayQ, setDisplayQ] = useState('questions')

  console.log('CI:not finished' + selectQPeriod);
  //Carl
  async function onCheckBoxClick(i: number, event: React.MouseEvent<HTMLInputElement, MouseEvent>) {

    await fetch(`${process.env.REACT_APP_BACKEND_URL}/checkedbox`, {
      method: 'post',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        user_id: userID,
        question_id: i,
        checked: event.currentTarget.checked ? 'true' : 'false'
      })
    })
    fetchCheckBoxClicked(userID!);
  }
  //Carl
  const fetchCheckBoxClicked = useCallback(async function (num: number) {
    const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/getcheckedbox`, {
      method: 'post',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        user_id: num
      })
    })
    const json = await res.json();
    dispatch(checkedOrNot(json));
  }, [dispatch])

  useEffect(() => {
    fetchCheckBoxClicked(userID!);
  }, [fetchCheckBoxClicked, userID])
  
  //Combine questions and checkbox test by carl and ted
  let combinedQuestionsArr = [];
  for (let index in questions) {
    let obj: any = questions[index];
    obj['marked'] = checkedBoxes[index];
    combinedQuestionsArr.push(obj)
  }
  return (
    <div className="Questions-List">
      <div className="Question-Cat">
        <div className="Checked-Display">
          <div className="Question-Cat-Button">
            Bookmark Question [{checkedBoxes.filter(checkedBox => checkedBox).length}/{questionByIDs.length}]
          </div>
        </div>
        <div className="All-Cat-Buttons">
          <button onClick={() => {
            setDisplayQ('questions')
            // dispatch(push('/'))
          }}>All Questions</button>
          <button onClick={() => {
            setDisplayQ('mostViewsQ')
            dispatch(fetchMostViewsQs())
            // dispatch(push('/question/most_views'))
          }}>Most Views</button>
          <button onClick={() => {
            setDisplayQ('hottestQ')
            dispatch(fetchHottestQs())
            // dispatch(push('/question/hottest'))
          }}>Hottest</button>
          <button onClick={() => {
            setDisplayQ('mostLikesQ')
            dispatch(fetchMostLikesQs())
            // dispatch(push('/question/most_likes'))
          }}>Most Likes</button>
          <button onClick={() => {
            setDisplayQ('answeredQ')
            dispatch(fetchAnsweredQs())
            // dispatch(push('/question/answered'))
          }}>Answered Q</button>
          <button onClick={() => {
            setDisplayQ('unansweredQ')
            dispatch(fetchUnansweredQs())
            // dispatch(push('/question/open'))
          }}>Open Q</button>
          <select name="postPeriod" onChange={e => setSelectQPeriod(e.currentTarget.value)}>
            <option >1 Month</option>
            <option value="7">1 Week</option>
            <option value="60">2 Months</option>
            <option value="90">3 Months</option>
            <option value="120">4 Months</option>
            <option value="150">5 Months</option>
            <option value="180">6 Months</option>
            <option value="">All Periods</option>
          </select>
        </div>
      </div>
      <div className="Questions-Wrap">
        {/* Carl searching */}
        {searchText !== '' && searchedQuestions.map((searchedQuestion, i) => (
          <div className="Question-Display" key={searchedQuestion.id}>
            <div className="CheckBox">
              <input type='checkbox' checked={checkedBoxes[i]} onClick={onCheckBoxClick.bind(null, searchedQuestion.id)} />
            </div>
            <div className="Each-Question">
              <div className="Question-Topic" onClick={() => {
                dispatch(readQ(searchedQuestion.id))
                dispatch(push(`/TheQuestion/${searchedQuestion.id}`))
              }}>{searchedQuestion.id}: {searchedQuestion.question_topic}
              </div>
              <div className="Question-Content">{searchedQuestion.question_content}</div>
            </div>
          </div>
        ))}
        {searchText === '' && <TotalQuestions displayQ={displayQ} />}
      </div>
    </div>
  )
}

export default AllTheQuestions;
