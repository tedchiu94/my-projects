import React from 'react';
import { useSelector } from 'react-redux'
import { RootState } from '../store';
import ReactMarkdown from 'react-markdown';

function AllTheReplies() {
    const replies = useSelector((state: RootState) => state.repliesQuestion);
    const disallowed:any = []

    return (
        <div className="Replies-List">
            <div className="Replies-Wrap">
                {
                    (replies['replies'].replies[0].reply_id) &&
                    replies.replies.replies.map((_reply, i) => (
                    <div className="Reply-Display" key={i}>
                        <div className="Reply-Topic">
                            Replier: { replies.replies.replies[i].reply_replier_nickname }
                        </div>
                        <div className="Reply-Content">
                            {replies.replies.replies[i].reply_upload_file &&
                                <img className="reply-img" src={`${process.env.REACT_APP_UPLOAD_URL}/${replies.replies.replies[i].reply_upload_file}`} alt="reply-upload-img" />
                            }
                            <ReactMarkdown className='Reply-Content-Markdown' source={replies.replies.replies[i].reply_content} disallowedTypes={disallowed}/>
                        </div>
                    </div>
                    ))
                }
            </div>
        </div>
    )
}

export default AllTheReplies;
