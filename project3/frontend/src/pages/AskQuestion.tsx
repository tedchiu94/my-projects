import React, { useEffect, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { closeAskQ } from '../redux/AskQuestion/actions';
import { useFormState } from 'react-use-form-state';
import { fetchQuestions } from '../redux/AllQuestions/actions';
import { push } from 'connected-react-router';
import { RootState } from '../store';
import { checkedOrNot } from '../redux/CheckBoxFavorite/action';


function AskQuestion() {

    const [formState, { text , textarea }] = useFormState();
    const userID = useSelector((state: RootState) => state.auth.user?.id);

    const dispatch = useDispatch();

    //Carl
    const fetchCheckBoxClicked = useCallback ( async function (num: number) {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/getcheckedbox`, {
          method: 'post',
          headers: {
            'Content-type': 'application/json'
          },
          body: JSON.stringify({
            user_id: num
          })
        })
        const json = await res.json();
        dispatch(checkedOrNot(json));
      },[dispatch])
    
      useEffect(() => {
        fetchCheckBoxClicked(userID!);
      }, [fetchCheckBoxClicked, userID])
      //Carl
    return (
        <>
            <div className="AskQHolder">
                <div className="AskQuestion">
                    <button className="CloseButton" onClick={() => {
                        dispatch(closeAskQ())
                        dispatch(push('/'))
                    }}>X</button>

                    <h1 className="AskQuestionHeader">Post your question </h1>
                    <form className="QuestionForm" onSubmit={async event => {
                        event.preventDefault();

                        let obj = {
                            form: formState.values,
                            id: userID
                        }
                        await fetch(`${process.env.REACT_APP_BACKEND_URL}/question/ask`, {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                                "Authorization":`Bearer ${localStorage.getItem('token')}`
                            },
                            body: JSON.stringify(obj)
                        })

                        await dispatch(closeAskQ())
                        await dispatch(fetchQuestions())
                        if (userID) {
                            await fetchCheckBoxClicked(userID)
                        }
                    }}>
                        <h2>Question:</h2>
                        <label>
                            <input className="QuestionTopic" placeholder="Please type your question here" {...text('question_topic')} required />
                        </label>
                        <h2>Give your question a tag:</h2>
                        <label>
                            <input className="QuestionTag" placeholder="Please name a tag for your question" {...text('question_tag')} required />
                        </label>
                        <h2>More details (if any):</h2>
                        
                        <label>
                            <textarea className="QuestionDetails"placeholder="Question details..." {...textarea('question_content')} required/>
                        </label>
                        <div>
                            <h2>File upload (if any):<button className="UploadButton">Select file</button></h2>
                        </div>

                        <input className="SubmitButton" type="submit" value="submit" />
                    </form>
                </div>
            </div>
        </>
    );
}

export default AskQuestion;
