//Carl
import { useFormState } from 'react-use-form-state'
import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { push } from 'connected-react-router'



function CreateAccount() {
    const [formState, { text, password, email, radio }] = useFormState();
    const [file, setFile] = useState<File | undefined>(undefined);
    const dispatch = useDispatch();

    return (
        <div className="item-area">
            <div className='outerContainer'>
                <div className="login-page">
                    <div className="form">
                        <form className='register-form' onSubmit={async (event) => {
                            event.preventDefault();

                            const formData = new FormData();
                            formData.append('username', formState.values.username)
                            formData.append('password', formState.values.password)
                            formData.append('email', formState.values.email)
                            formData.append('role', formState.values.role)
                            formData.append('nickname', formState.values.nickname)

                            if (file) {
                                formData.append('photo', file);
                            }
                            console.log(formData)
                            await fetch(`${process.env.REACT_APP_BACKEND_URL}/userac`, {
                                method: 'post',
                                body: formData
                            })
                            dispatch(push('/login'));

                        }}>
                            <input {...text('username')} placeholder="username" required/>
                            <input {...text('nickname')} placeholder="nickname" required/>
                            <input {...password('password')} placeholder="password" required/>
                            <input {...email('email')} placeholder="email address" required/>
                            <input type="file" onChange={event => setFile(event.currentTarget.files?.[0])} />
                            <div>
                                <label>Student <input {...radio('role', 'student')} required/></label>
                                <label>Tutor  <input {...radio('role', 'tutor')} required/></label>
                            </div>
                            <button>create account</button>
                            <p className="Toggle-Login"><button onClick={
                                () => dispatch(push('/login'))}>Already registered? Sign In</button></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CreateAccount
//Carl