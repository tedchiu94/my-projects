import React from 'react'
import { useFormState } from 'react-use-form-state';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../store';
import UserRecordPage from './UserRecordQuestionsPage';
import { push } from 'connected-react-router';

export default function ProfilePage() {
    const [formState, { text, password }] = useFormState();
    const users = useSelector((state: RootState) => state.auth.user);
    const token = useSelector((state: RootState) => state.auth.token);
    const questionByIDs = useSelector((state: RootState) => state.question.questionIds);
    const questions = useSelector((state: RootState) => questionByIDs.map(id => state.question.questionById[id]));
    const dispatch = useDispatch();

    let counts = 0;
    for (let question of questions) {
        if (question.creator_id === users?.id) {
            counts += +((question as any).likes_count)
        }
    }

    return (
        <>
            <div className="Profile-Header">
                <h1>Your Account</h1>
            </div>
            <div className="UserPage-HomePage">
                <button className="UserPage-HomePage-Button" onClick={() => dispatch(push(`/`))}>HomePage</button>
            </div>
            <div className="Details-Wrapper">
                <div className="Wrapper">
                    {users && users.profilepic &&
                    <img className="Icon-Bubble" src={`${process.env.REACT_APP_UPLOAD_URL}/${users.profilepic}`} alt="logo" />
                    }
                    <div className="Slogan-Box">
                        <label><input type="text" className="Slogon-Form"
                            placeholder="Call Me PROGRAMMER!" /></label>
                    </div>
                </div>
                <form onSubmit={async (event) => {
                    event.preventDefault();

                    let obj = {
                        form: formState.values,
                        id: users?.id
                    }

                    await fetch(`${process.env.REACT_APP_BACKEND_URL}/AmendUserAC`, {
                        method: 'post',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${token}`
                        },
                        body: JSON.stringify(obj)
                    })
                }}>
                    <div className="Info-Wrapper">
                        <div className="Info-Detail">
                            <h3>Nickname :</h3>
                        </div>
                        <div className="Slogan-Box">
                            <label><input {...text('nickname')} className="Info-Form" 
                                placeholder="change nickname" required/></label>
                        </div>
                    </div>
                    <div className="Info-Wrapper">
                        <div className="Info-Detail">
                            <h3>Password :</h3>
                        </div>
                        <div className="Slogan-Box">
                            <label><input {...password('password')} className="Info-Form"
                                placeholder="change password" required /></label>
                        </div>
                    </div>
                    <div className="Info-Wrapper">
                        <div className="Info-Detail">
                            <h3>Email :</h3>
                        </div>
                        <div className="Slogan-Box">
                            <label><input type="text" className="Info-Form" value={users?.email}
                                placeholder="hello@gmail.com" /></label>
                        </div>
                    </div>
                    <div className="Button-Wrapper">
                        <button className="">save changes</button>
                    </div>
                </form>
            </div>
            <div className="Profile-Header">
                <h1>Status </h1>
                <h3>scores : {counts} </h3>
            </div>
            <UserRecordPage />
        </>
    )
}