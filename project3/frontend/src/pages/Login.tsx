//Carl
import { useFormState } from 'react-use-form-state'
import React from 'react';
import { useDispatch } from 'react-redux';
import { login } from '../auth/thunk';
import { push } from 'connected-react-router';

export default function Login() {
    const [formState, { text, password }] = useFormState();
    const dispatch = useDispatch();

    return (
        <div className="item-area">
            <div className='outerContainer'>
                <div className="login-page">
                    <div className="form">
                        <form className='register-form' onSubmit={(event) => {
                            event.preventDefault();
                            dispatch(login(formState.values.username, formState.values.password))
                        }}>
                            <input {...text('username')} placeholder="username" />
                            <input {...password('password')} placeholder="password" />
                            <button>login</button>
                            <p className="Toggle-Login"><button onClick={
                                () => dispatch(push('/CreateAccount'))}>Not registered? Create an account</button></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )

}
