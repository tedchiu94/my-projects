import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../store';
import { fetchQuestions, Question } from '../redux/AllQuestions/actions';
import { Link, Route, useParams } from 'react-router-dom';
import TheQuestion from './TheQuestion';
import { push } from 'connected-react-router';
import HomePage from './HomePage';
import { readQ } from '../redux/TheQuestion/actions';
import { checkedOrNot } from '../redux/CheckBoxFavorite/action';


function AllTheQuestionsCheckBox() {

    const dispatch = useDispatch()

    const questionByIDs = useSelector((state: RootState) => state.question.questionIds)
    const questions = useSelector((state: RootState) => questionByIDs.map(id => state.question.questionById[id]))
    const userID = useSelector((state: RootState) => state.auth.user?.id)
    const checkedBoxes = useSelector((state: RootState) => state.checkBox)
   

    useEffect(() => {
        dispatch(fetchQuestions());
    }, [dispatch])

    //Carl
    async function onCheckBoxClick(i: number, event: React.MouseEvent<HTMLInputElement, MouseEvent>) {
        // console.log(i)
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/checkedbox`, {
            method: 'post',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                user_id: userID,
                question_id: i,
                checked: event.currentTarget.checked ? 'true' : 'false'
            })
        })

        fetchCheckBoxClicked(userID!);
    }

    async function fetchCheckBoxClicked(num: number) {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/getcheckedbox`, {
            method: 'post',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                user_id: num
            })
        })
        const json = await res.json();
        console.log(json);

        dispatch(checkedOrNot(json));
    }

    useEffect(() => {
        fetchCheckBoxClicked(userID!);
    }, [userID])
    //Carl

    return (
        <div className="Questions-List">
            <div className="Question-Cat">
                <button className="Question-Cat-Button">Views</button>
                <button className="Question-Cat-Button">Hottest</button>
                <button className="Question-Cat-Button">Most Likes</button>
                <button className="Question-Cat-Button">This Week</button>
                <button className="Question-Cat-Button">Answered Q</button>
                <button className="Question-Cat-Button">Open Q</button>
            </div>
            <div className="Questions-Wrap">
                {
                    questions.map((question, i) => (
                        <div className='checkbox-display'>
                            <input type='checkbox' checked={checkedBoxes[i]} onClick={onCheckBoxClick.bind(null, i + 1)} />
                        
                            <div className="Question-Display" key={question.id}>
                                <div className="Question-Topic" onClick={() => {
                                    dispatch(readQ(question.id))
                                    dispatch(push(`/TheQuestion/${question.id}`))
                                }}>{question.id}: {question.question_topic}
                                </div>
                                <div className="Question-Content">{question.question_content}</div>
                            </div>
                        </div>
                    ))
                }
            </div>
        </div>
    )
}

export default AllTheQuestionsCheckBox;






 // const resPut = await fetch(`${process.env.REACT_APP_BACKEND_URL}/checkedbox/${i}`, {
        //     method: 'put',
        //     headers: {
        //         'Content-type': 'application/json'
        //     },
        //     body: JSON.stringify({
        //         checked: event.currentTarget.checked ? 'true': 'false'
        //     })
        // })
        // const json = await resPut.json();