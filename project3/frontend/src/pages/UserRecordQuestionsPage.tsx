import React, { useState, useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../store";
import { Reply } from "../redux/AllTheReplies/actions";
import { readQ } from "../redux/TheQuestion/actions";
import { push } from "connected-react-router";

export default function UserRecordPage() {

    const userID = useSelector((state: RootState) => state.auth.user?.id)
    const [page, setPage] = useState('bookmarked');
    const [favQuestions, setFavQuestions] = useState<null | Reply[]>(null);
    const [postedQuestions, setPostedQuestions] = useState<null | PostedQuestions[]>(null);
    const [checkbox, setCheckbox] = useState<boolean[] | null>(null);
    const [isOpen, setIsOpen] = useState(false);
    const dispatch = useDispatch();


    const loadFavQuestion = useCallback (async function () {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/FavQuestions?q=${userID}`);
        const json = await res.json();
        // console.log(Object.values(json));
        const favQ: Reply[] = Object.values(json);
        setFavQuestions(favQ);
    },[userID])

    interface PostedQuestions {
        question_id : number;
        question_topic : string;
        question_content : string
    }

    const loadPostedQuestion = useCallback (async function () {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/PostedQuestions?q=${userID}`);
        const json = await res.json();
        // console.log(json);
        setPostedQuestions(json);
    }, [userID])

    async function onCheckBoxClick(i: number, event: React.MouseEvent<HTMLInputElement, MouseEvent>) {

        await fetch(`${process.env.REACT_APP_BACKEND_URL}/checkedbox`, {
            method: 'post',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                user_id: userID,
                question_id: i,
                checked: event.currentTarget.checked ? 'true' : 'false'
            })
        })
        loadFavQuestion();
        fetchCheckBoxClicked(userID!);
    }

    async function fetchCheckBoxClicked(num: number) {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/getUserRecordCheckedbox`, {
            method: 'post',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                user_id: num
            })
        })
        const json = await res.json();
        setCheckbox(json);
    }

    useEffect(() => {
        fetchCheckBoxClicked(userID!);
    }, [userID])

    useEffect(() => {
        loadFavQuestion();
    }, [loadFavQuestion])

    useEffect(() => {
        loadPostedQuestion();
    }, [loadPostedQuestion])

    return (
        <>
            {/* 1st part: fav-q and posted-q buttons */}
            <h1 className="Profile-Header">
                <div className="item-navBar">
                    <span><button className="UserPage-Button" onClick={() => setPage('bookmarked')}>Bookmarked</button></span>
                    <span><button className="UserPage-Button" onClick={() => setPage('postedQuestions')}>Posted Questions </button></span>
                </div>
            </h1>
            <h3 className="Profile-Header">{page+" :"}</h3>
            {/* 2nd part: rendering on the lower part */}
            {page === 'bookmarked' && favQuestions !== null &&
                favQuestions.map((favQuestion, i) => (
                    <>
                        <div className="Each-UserPage-Question Color-Hover-Change">
                            <div className="Each-UserPage-Question-Button">
                                <button onClick={() => {
                                    if (isOpen) {
                                        setIsOpen(false)
                                    } else {
                                        setIsOpen(true)
                                    }
                                }}>{isOpen ? '-' : '+'}</button>
                                {isOpen && <input className="CheckBox" type='CheckBox' checked={checkbox !== null && checkbox[i]}
                                    onClick={onCheckBoxClick.bind(null, favQuestion.question_id)} />}
                            </div>
                            <div className="Each-Question">
                                <div>
                                    {/* this is from TotalQuestions.tsx */}
                                    <div className="Question-Topic" onClick={() => {
                                        dispatch(readQ(favQuestion.question_id))
                                        dispatch(push(`/question/${favQuestion.question_id}`))
                                    }}><h3>{favQuestion.question_id}: {favQuestion.question_topic}</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </>
                )
                )
            }
            {page === 'postedQuestions' && postedQuestions !== null &&
                postedQuestions.map((postedQuestion) => (
                    <>
                        <div className="Each-UserPage-Question Color-Hover-Change">
                        <div className="Each-Question">
                                <div>
                                    <div className="Question-Topic" onClick={() => {
                                        dispatch(readQ(postedQuestion.question_id))
                                        dispatch(push(`/question/${postedQuestion.question_id}`))
                                    }}><h3>{postedQuestion.question_id}: {postedQuestion.question_topic}</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </>
                ))
            }
            {/* 3rd part: button to homepage */}
            <div className="UserPage-HomePage">
                <button className="UserPage-HomePage-Button" onClick={() => dispatch(push(`/`))}>HomePage</button>
            </div>


        </>
    )
}



// from TotalQuestions.tsx, copy those checkbox func codes to here;
// when a box is unchecked, a post/put fetch is sent to update table: with question_id : favQuestion.question_id;
// since controlled variable for checkboxes still exists here, an arr of boolean `get` from a backend route is still needed;
// but this time, length of boolean arr isn't all the questions, but only this user's fav q;
// useEffect is needed to get that boolean arr when this component is initially rendered