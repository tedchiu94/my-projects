import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from '../store'
import { fetchQuestions } from '../redux/AllQuestions/actions'
import NavTop from './Nav/NavTop'
import NavLeft from './Nav/NavLeft'
import AllTheQuestions from './AllTheQuestions'
import { askQ } from '../redux/AskQuestion/actions'
import AskQuestion from './AskQuestion'
import TheQuestion from './TheQuestion'
import { useParams } from 'react-router-dom'
import { readQ } from '../redux/TheQuestion/actions'

function HomePage() {

  const dispatch = useDispatch()
  const askQRequest = useSelector((state: RootState) => state.askQ.askQ)
  const readQuestionID = useSelector((state:RootState) => state.readQ.questionID)
  const params = useParams<{id:string}>()

  useEffect(() => {
    askQ()
  },[askQRequest])

  useEffect(() => {
      let questionID = + params.id || 0
      if (readQuestionID !== questionID){
          dispatch(readQ(readQuestionID))
      }
  },[params.id,dispatch,readQuestionID])

  useEffect(() => {
    dispatch(fetchQuestions())
  }, [dispatch])

  return (
    <>
      <NavTop />
      <div className="Content-Holder">
          <NavLeft />
          {<AllTheQuestions />}
          {askQRequest === "yes" && <AskQuestion />}
          {readQuestionID !== 0 && <TheQuestion  />}    
      </div>
    </>    
  )
}

export default HomePage;
