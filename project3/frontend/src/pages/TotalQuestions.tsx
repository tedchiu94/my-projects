import React, { useEffect, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../store'
import { readQ } from '../redux/TheQuestion/actions'
import { push } from 'connected-react-router'
import { checkedOrNot } from '../redux/CheckBoxFavorite/action'
import { fetchQuestions, Question } from '../redux/AllQuestions/actions'
import whiteHeart from '../images/whiteheart.png'
import redHeart from '../images/redheart.png'

function TotalQuestions(props: {
  displayQ: string
}) {
  const cohortNow = useSelector((state: RootState) => state.question.cohortNum)
  const questionByIDs = useSelector((state: RootState) => state.question.questionIds)
  const questions = useSelector((state: RootState) => questionByIDs.map(id => state.question.questionById[id]))
  const mostLikesQByIDs = useSelector((state: RootState) => state.question.likedQIds)
  const mostLikesQ = useSelector((state: RootState) => mostLikesQByIDs.map(id => state.question.questionById[id]))
  const mostViewsQByIDs = useSelector((state: RootState) => state.question.viewedQIds)
  const mostViewsQ = useSelector((state: RootState) => mostViewsQByIDs.map(id => state.question.questionById[id]))
  const hottestQByIDs = useSelector((state: RootState) => state.question.hottestQIds)
  const hottestQ = useSelector((state: RootState) => hottestQByIDs.map(id => state.question.questionById[id]))
  const answeredQByIDs = useSelector((state: RootState) => state.question.answeredQIDs)
  const answeredQ = useSelector((state: RootState) => answeredQByIDs.map(id => state.question.questionById[id]))
  const unansweredQByIDs = useSelector((state: RootState) => state.question.unansweredQIDs)
  const unansweredQ = useSelector((state: RootState) => unansweredQByIDs.map(id => state.question.questionById[id]))
  const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated)
  const token = useSelector((state: RootState) => state.auth.token)

  //switch case
  let questionType: Question[];
  if (props.displayQ === "questions") {
    questionType = questions
  } else if (props.displayQ === "mostViewsQ") {
    questionType = mostViewsQ
  } else if (props.displayQ === 'hottestQ') {
    questionType = hottestQ
  } else if (props.displayQ === "mostLikesQ") {
    questionType = mostLikesQ
  } else if (props.displayQ === 'answeredQ') {
    questionType = answeredQ
  } else if (props.displayQ === 'unansweredQ') {
    questionType = unansweredQ
  } else {
    questionType = questions
  }

  const userID = useSelector((state: RootState) => state.auth.user?.id)
  const checkedBoxes = useSelector((state: RootState) => state.checkBox)
  const dispatch = useDispatch();
  //Carl
  async function onCheckBoxClick(i: number, event: React.MouseEvent<HTMLInputElement, MouseEvent>) {
    await fetch(`${process.env.REACT_APP_BACKEND_URL}/checkedbox`, {
      method: 'post',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        user_id: userID,
        question_id: Object.keys(questions).length - i + 1,
        checked: event.currentTarget.checked ? 'true' : 'false'
      })
    })
    fetchCheckBoxClicked(userID!);
  }

  const fetchCheckBoxClicked = useCallback(async function (num: number) {
    const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/getcheckedbox`, {
      method: 'post',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        user_id: num
      })
    })
    const json = await res.json();
    dispatch(checkedOrNot(json));
  }, [dispatch])

  useEffect(() => {
    fetchCheckBoxClicked(userID!);
  }, [fetchCheckBoxClicked, userID])
  //Carl
  return (
    <>
      {cohortNow !== "All" &&
        questionType.filter(question => question.cohort_source === cohortNow).map((question, i) => (
          <div className="Question-Display" key={question.id}>
            <div className="Q-Status">
        <div className="Q-Status-Info">Views   ({question.views})</div>
              <div className="Q-Status-Info">Likes   ({question.likes_count})</div>
              <div className="Q-Status-Info">Replies ({question.replies_count})</div>
            </div>
            <div className="Features">
              <div className="Feature-click">
                <input className="CheckBox" type='CheckBox' checked={checkedBoxes[i]} onClick={onCheckBoxClick.bind(null, i)} />
              </div>
              {isAuthenticated && question.liked === "0" &&
                <div className="Feature-click">
                  <img className="heartIcon" src={whiteHeart} alt="heartIcon"
                    onClick={async () => {
                      await fetch(`${process.env.REACT_APP_BACKEND_URL}/question/${question.id}/like`, {
                        method: 'PUT',
                        headers: {
                          Authorization: `Bearer ${token}`
                        }
                      })
                      dispatch(fetchQuestions())
                    }} />
                </div>
              }
              {isAuthenticated && question.liked === "1" &&
                <div className="Feature-click">
                  <img className="heartIcon" src={redHeart} alt="heartIcon"
                    onClick={async () => {
                      await fetch(`${process.env.REACT_APP_BACKEND_URL}/question/${question.id}/unlike`, {
                        method: 'PUT',
                        headers: {
                          Authorization: `Bearer ${token}`
                        }
                      })
                      dispatch(fetchQuestions())
                    }} />
                </div>
              }
            </div>
            <div className="Each-Question">
              <div>
                <div className="Question-Topic" onClick={() => {
                  dispatch(readQ(question.id))
                  dispatch(push(`/question/${question.id}`))
                }}>{question.id}: {question.question_topic}
                </div>
                <div className="Question-Content">
                  <div>
                    {/* <span className="tag">tag</span>
                    <span className="tag">tag</span> */}
                  </div>
                  {isAuthenticated && question.liked === "0" &&
                    <div className="Feature-click">
                      <img className="heartIcon" src={whiteHeart} alt="heartIcon"
                        onClick={async () => {
                          await fetch(`${process.env.REACT_APP_BACKEND_URL}/question/${question.id}/like`, {
                            method: 'PUT',
                            headers: {
                              Authorization: `Bearer ${token}`
                            }
                          })
                          dispatch(fetchQuestions())
                        }} />
                    </div>
                  }
                  {isAuthenticated && question.liked === "1" &&
                    <div className="Feature-click">
                      <img className="heartIcon" src={redHeart} alt="heartIcon"
                        onClick={async () => {
                          await fetch(`${process.env.REACT_APP_BACKEND_URL}/question/${question.id}/unlike`, {
                            method: 'PUT',
                            headers: {
                              Authorization: `Bearer ${token}`
                            }
                          })
                          dispatch(fetchQuestions())
                        }} />
                    </div>
                  }
                  <div>
                    <div className="Post-Date">{question.created_at}</div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        ))
      }
      {cohortNow === "All" &&
        questionType.map((question, i) => (
          <div className="Question-Display" key={question.id}>
            <div className="Q-Status">
        <div className="Q-Status-Info">Views   ({question.views})</div>
              <div className="Q-Status-Info">Likes   ({question.likes_count})</div>
              <div className="Q-Status-Info">Replies ({question.replies_count})</div>
            </div>
            <div className="Features">
              <div className="Feature-click">
                <input className="CheckBox" type='CheckBox' checked={checkedBoxes[i]} onClick={onCheckBoxClick.bind(null, i + 1)} />
              </div>
              {isAuthenticated && question.liked === "0" &&
                <div className="Feature-click">
                  <img className="heartIcon" src={whiteHeart} alt="heartIcon"
                    onClick={async () => {
                      await fetch(`${process.env.REACT_APP_BACKEND_URL}/question/${question.id}/like`, {
                        method: 'PUT',
                        headers: {
                          Authorization: `Bearer ${token}`
                        }
                      })
                      dispatch(fetchQuestions())
                    }} />
                </div>
              }
              {isAuthenticated && parseInt(question.liked as any) > 0 &&
                <div className="Feature-click">
                  <img className="heartIcon" src={redHeart} alt="heartIcon"
                    onClick={async () => {
                      await fetch(`${process.env.REACT_APP_BACKEND_URL}/question/${question.id}/unlike`, {
                        method: 'PUT',
                        headers: {
                          Authorization: `Bearer ${token}`
                        }
                      })
                      dispatch(fetchQuestions())
                    }} />
                </div>
              }
            </div>
            <div className="Each-Question">
              <div>
                <div className="Question-Topic" onClick={() => {
                  dispatch(readQ(question.id))
                  dispatch(push(`/question/${question.id}`))
                }}>{question.id}: {question.question_topic}
                </div>
                <div className="Question-Content">
                  <div>
                    {/* <span className="tag">tag</span>
                    <span className="tag">tag</span> */}
                  </div>
                  <div>
                    <div className="Post-Date">{question.created_at}</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))
      }
    </>
  )
}

export default TotalQuestions
