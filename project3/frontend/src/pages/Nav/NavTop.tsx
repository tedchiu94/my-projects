import React, { useState, useEffect } from 'react'
import tecky from '../../images/tecky.png'
// import message from '../../images/message-icon.png'
import setting from '../../images/setting-icon.png'
import { Link } from 'react-router-dom';
import { askQ } from '../../redux/AskQuestion/actions';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../store';
import { logout } from '../../auth/action';
import { push } from 'connected-react-router';
import { loadedSearchedQuestions } from '../../redux/SearchedQuestions/action';
import { searchingText } from '../../redux/SearchText/action';

function NavTop() {
    // Carl
    const [openBox, setOpenBox] = useState(false);
    const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated);
    const username = useSelector((state: RootState) => state.auth.user?.username);
    // const [searchText, setSearchText] = useState('');
    const searchText = useSelector((state: RootState) => state.searchText);
    // const userPoints = useSelector((state: RootState) => state.auth.user?.question_points)
    const users = useSelector((state: RootState) => state.auth.user);
    const questionByIDs = useSelector((state: RootState) => state.question.questionIds)
    const questions = useSelector((state: RootState) => questionByIDs.map(id => state.question.questionById[id]))

    const dispatch = useDispatch();

    useEffect(() => {
        async function reload() {
            const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/searchQ?q=${searchText}`)
            const json = await res.json()
            // console.log(json)
            dispatch(loadedSearchedQuestions(json))
        }

        const timer = setTimeout(
            reload, 700);
        return () => clearTimeout(timer);
    }, [searchText, dispatch])
    // Carl

    let counts = 0;
    for (let question of questions) {
        if (question.creator_id === users?.id) {
            counts += +((question as any).likes_count)
        }
    }

    return (
        <div className="Top-Nav">
            <div className="Top-Nav-left">
                <img src={tecky} className="Tecky-logo" alt="logo" />
                Tecky Academy
            </div>
            <div className="Top-Nav-middle">
                <div>{username}</div>
                <div>Rank Points: {counts}</div>
            </div>
            <div className="Top-Nav-right">


                <input value={searchText} onChange={async e => {
                    dispatch(searchingText(e.currentTarget.value))

                    // const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/searchQ?q=${e.currentTarget.value}`)
                    // const json = await res.json()
                    // console.log(json)
                    // dispatch(loadedSearchedQuestions(json))
                }} type="search" className="Nav-search" placeholder="Search..."></input>

                {/* <img src={message} className="Nav-logo" alt="logo" />
                <h5>1</h5> */}

                <button className="AskQ-logo" onClick={() => {
                    dispatch(askQ())
                    dispatch(push('/question/ask'))
                }}>Post a question</button>


                {/* Carl */}
                <img src={setting} className="Nav-logo" alt='setting-img' onClick={() => {
                    if (openBox) {
                        setOpenBox(false)
                    } else {
                        setOpenBox(true)
                    }
                }} />
                {
                    openBox && <>
                        <div className="position-relative">
                            <div className="position-absolute">
                                {isAuthenticated && <div className="Welcome-User">Welcome back, {username}</div>}
                                {!isAuthenticated && <button>
                                    <Link to='/login'>Login</Link>
                                </button>}
                                {isAuthenticated && <button onClick={() => {
                                    localStorage.setItem('token', '');
                                    dispatch(logout());
                                    dispatch(push(`/login`))
                                }}>logout</button>}
                                {!isAuthenticated && <Link to='/createaccount'>Create</Link>}
                                {isAuthenticated && <Link to='/profile'>Profile</Link>}
                                {/* {isAuthenticated && <Link to='/userRecord'>UserRecord</Link>} */}
                                {/* <Link to='/AskQuestion'>AskQuestion</Link> */}
                            </div>
                        </div>
                    </>
                }
            </div>
            {/* Carl */}
        </div>
    )
}

export default NavTop;