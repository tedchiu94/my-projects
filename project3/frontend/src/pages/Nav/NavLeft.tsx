import React, { useState } from 'react'
import { useDispatch } from 'react-redux';
import { pickCohort } from '../../redux/AllQuestions/actions';

function NavLeft() {
    const dispatch = useDispatch();
    const [cohort, setCohort] = useState(true)
    const [rank, setRank] = useState(true)
    const [award, setAward] = useState(true)

    let cohortOptions: (string | number)[] =
        ["All", 9, 10,
            11, 12, 13,
            14, 15, 16]

    return (
        <>
            <div className="Left-Nav">
                <div className="Left-Nav-Cat" onClick={() => {
                    setCohort(!cohort)
                }
                }
                >Room of Cohorts{cohort ? " +" : " -"}</div>
                {
                    cohort &&
                    <>
                        <div className="Left-Nav-Options">
                            {
                                cohortOptions.map(cohort => (
                                    <div key={cohort} className="option" onClick={() => {
                                        dispatch(pickCohort(cohort))
                                    }
                                    }>Cohort {cohort}</div>
                                )
                                )
                            }
                        </div>
                    </>
                }
                <div className="Left-Nav-Cat" onClick={() =>
                    setRank(!rank)}>Table of Rankings{rank ? " +" : " -"}</div>
                {
                    rank &&
                    <>
                        <div className="Left-Nav-Options">
                            <div className="option">Questions</div>
                            <div className="option">Users</div>
                        </div>
                    </>
                }
                <div className="Left-Nav-Cat" onClick={() =>
                    setAward(!award)}>List of Awards{award ? " +" : " -"}</div>
                {
                    award &&
                    <>
                        <div className="Left-Nav-Options">
                            <div className="option">Awards of August</div>
                            <div className="option">Awards of September</div>
                        </div>
                    </>
                }
            </div>
        </>
    )
}

export default NavLeft;
