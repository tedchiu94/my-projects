//Carl
import { ThunkDispatch, RootState } from "../store";
import { loginSuccess, logout, getUser } from "./action";

export function login(username: string, password: string) {
    return async (dispatch: ThunkDispatch) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/auth/login`, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username, password
            })
        })

        const json = await res.json();

        if (json.token !== null) {
            localStorage.setItem('token', json.token);
            dispatch(loginSuccess(json.token));
            dispatch(restoreLogin());
        } else {
            dispatch(logout())
        }
    }
}

export function restoreLogin() {
    return async (dispatch: ThunkDispatch, getState: ()=>RootState) => {
        const token = getState().auth.token;

        if (token === null) {
            dispatch(logout()); //isAuthenticated => false
            return;
        }

        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/user/current`, {
            headers: {
                'Content-type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
        const json = await res.json();

        if (res.status !== 200) {
            dispatch(logout()); 
            return;
        }

        else if (res.status === 200) {
            dispatch(loginSuccess(token))
            dispatch(getUser(json))
        }
    }
}
//Carl