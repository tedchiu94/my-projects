//Carl
import { User } from "./reducer"

export function loginSuccess(token:string) {
    return {
        type: '@@AUTH/LOGINSUCCESS' as '@@AUTH/LOGINSUCCESS',
        token
    }
}

export function getUser(user:User) {
    return {
        type: '@@AUTH/GETUSER' as '@@AUTH/GETUSER',
        user
    }
}
    
export function logout() {
    return {
        type: '@@AUTH/LOGOUT' as '@@AUTH/LOGOUT'
    }
}

export type AuthActions = ReturnType<typeof loginSuccess | typeof getUser |typeof logout>
//Carl
