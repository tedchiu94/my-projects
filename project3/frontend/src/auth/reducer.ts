//Carl
import { AuthActions } from "./action";

export interface AuthState {
    token: string | null;
    user: User | null;
    isAuthenticated: boolean | null;
}

export interface User {
    id: number;
    username: string;
    email: string;
    profilepic: string;
    nickname: string
    question_points: number;
}

const initialState: AuthState = {
    token: localStorage.getItem('token'),
    user: null,
    isAuthenticated: null,
}

export const authReducer = (state:AuthState = initialState, action: AuthActions):AuthState => {
    switch (action.type) {
        case "@@AUTH/LOGINSUCCESS":
            {
                return {
                    ...state,
                    token: action.token,
                    isAuthenticated: true
                }
            }
        
        case "@@AUTH/GETUSER":
            {
                return {
                    ...state,
                    user: action.user
                }
            }
        
        case "@@AUTH/LOGOUT":
            {
                return {
                    ...state,
                    token: null,
                    user: null,
                    isAuthenticated: false
                }
            }

    }
    return state;
}
//Carl