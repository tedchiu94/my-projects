// action creator

export interface SearchedQuestion {
    id:               number;
    question_topic:   string;
    question_tag:     string;
    question_content: string;
    created_at:       string;
    updated_at:       string;
    creator_id:       number
}

//Carl searching
export function loadedSearchedQuestions(questions: SearchedQuestion[]) {
    return {
        type: '@@question/LOADED_SEARCHED_QUESTIONS' as '@@question/LOADED_SEARCHED_QUESTIONS',
        questions
    }
}
//Carl searching

export type SearchedQuestionAction = ReturnType<typeof loadedSearchedQuestions>



