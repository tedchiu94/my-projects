import { SearchedQuestion, SearchedQuestionAction } from "./action";

export interface SearchedQuestionState {
    questionIds: number[];
    questionById: {
        [id: string]: SearchedQuestion
    }
}

const initialState = {
    questionIds: [],
    questionById: {}
}

export const searchedQuestionReducer = (state: SearchedQuestionState = initialState, action: SearchedQuestionAction):
    SearchedQuestionState => {
    switch (action.type) {
        //Carl search
        case '@@question/LOADED_SEARCHED_QUESTIONS':
            {
                const newQuestionById: {
                    [id: string]: SearchedQuestion
                } = {}
                for (const question of action.questions) {
                    newQuestionById[question.id] = question;
                }
                return {
                    questionIds: action.questions.map(question => question.id),
                    questionById: newQuestionById
                }
            }
        //Carl search

        default:
            return state;
    }
}