import { QuestionActions, Question } from "./actions"

export interface QuestionState {
    questionIds: number[];
    likedQIds: number[];
    viewedQIds: number[];
    hottestQIds: number[];
    answeredQIDs: number[];
    unansweredQIDs: number[];
    cohortNum: string | number;
    questionById: {
        [id: string]: Question
    },
    
}

const initialState = {
    questionIds: [],
    likedQIds: [],
    viewedQIds: [],
    hottestQIds: [],
    answeredQIDs: [],
    unansweredQIDs: [],
    questionById: {},
    cohortNum: "All",
}

export const questionReducer = (state: QuestionState = initialState, action: QuestionActions): QuestionState => {
    switch (action.type) {
        case '@@question/LOADED_QUESTIONS':
            {   //console.log(action)
                const newQuestionById: {
                    [id: string]: Question
                } = {...state.questionById}
                for (const question of action.questions) {
                    newQuestionById[question.id] = question;
                }
                return {  
                    ...state,
                    questionIds: action.questions.map(question => question.id),
                    questionById: newQuestionById
                }
            }
        case '@@mostLikes/LOADED_MOST_LIKES_Q':
            {   
                const newQuestionById: {
                    [id: string]: Question
                } = {...state.questionById}
                for (const question of action.questions) {
                    newQuestionById[question.id] = question;
                }
                return {
                    ...state,
                    likedQIds: action.questions.map(question => question.id),
                    questionById: newQuestionById
                }
            }
        case '@@mostViews/LOADED_MOSTVIEWS':
            {   
                const newQuestionById: {
                    [id: string]: Question
                } = {...state.questionById}
                for (const question of action.questions) {
                    newQuestionById[question.id] = question;
                }
                return {
                    ...state,
                    viewedQIds: action.questions.map(question => question.id),
                    questionById: newQuestionById
                }
            }
        case '@@hottestQ/LOADED_HOTTEST_Q':
            {   
                const newQuestionById: {
                    [id: string]: Question
                } = {...state.questionById}
                for (const question of action.questions) {
                    newQuestionById[question.id] = question;
                }
                return {
                    ...state,
                    hottestQIds: action.questions.map(question => question.id),
                    questionById: newQuestionById
                }
            }
        case '@@answeredQ/LOADED_ANSWERED_Q':
            {   
                const newQuestionById: {
                    [id: string]: Question
                } = {...state.questionById}
                for (const question of action.questions) {
                    newQuestionById[question.id] = question;
                }
                return {
                    ...state,
                    answeredQIDs: action.questions.map(question => question.id),
                    questionById: newQuestionById
                }
            }
        case '@@unansweredQ/LOADED_UNANSWERED_Q':
            {   
                const newQuestionById: {
                    [id: string]: Question
                } = {...state.questionById}
                for (const question of action.questions) {
                    newQuestionById[question.id] = question;
                }
                return {
                    ...state,
                    unansweredQIDs: action.questions.map(question => question.id),
                    questionById: newQuestionById
                }
            }
        
        case "pickCohort":
            {
                return {
                    ...state,
                    cohortNum: action.cohortNum}
            }

        
        default:
            return state;
    }
}


// export const mostViewsQReducer = (state: MostViewsQState = initialState, action: QuestionActions): QuestionState => {
//     switch (action.type) {
//         case '@@mostViews/LOADED_MOSTVIEWS':
//             {
//                 const newMostViewsQById: {
//                     [id: string]: Question
//                 } = {}
//                 for (const question of action.mostViewsQ) {
//                     newMostViewsQById[question.id] = question;
//                 }
//                 return {
//                     questionIds: action.mostViewsQ.map(mostViews => mostViews.id),
//                     questionById: newMostViewsQById
//                 }
//             }           
//         default:
//             return state;
//     }
// }


// export const hottestQReducer = (state: HottestQState = initialState, action: QuestionActions): QuestionState => {
//     switch (action.type) {
//         case '@@hottestQ/LOADED_HOTTEST_Q':
//             {
//                 const newHottestQById: {
//                     [id: string]: Question
//                 } = {}
//                 for (const question of action.hottestQ) {
//                     newHottestQById[question.id] = question;
//                 }
//                 return {
//                     questionIds: action.hottestQ.map(hottest => hottest.id),
//                     questionById: newHottestQById
//                 }
//             }         
//         default:
//             return state;
//     }
// }



// export const answeredQReducer = (state: AnsweredQState = initialState, action: QuestionActions): QuestionState => {
//     switch (action.type) {
//         case '@@answeredQ/LOADED_ANSWERED_Q':
//             {
//                 const newAnsweredQById: {
//                     [id: string]: Question
//                 } = {}
//                 for (const question of action.answeredQ) {
//                     newAnsweredQById[question.id] = question;
//                 }
//                 return {
//                     questionIds: action.answeredQ.map(answered => answered.id),
//                     questionById: newAnsweredQById
//                 }
//             }
//         default:
//             return state;
//     }
// }

// export const unansweredQReducer = (state: UnansweredQState = initialState, action: QuestionActions): QuestionState => {
//     switch (action.type) {
//         case '@@unansweredQ/LOADED_UNANSWERED_Q':
//             {
//                 const newUnansweredQById: {
//                     [id: string]: Question
//                 } = {}
//                 for (const question of action.unansweredQ) {
//                     newUnansweredQById[question.id] = question;
//                 }
//                 return {
//                     questionIds: action.unansweredQ.map(unanswered => unanswered.id),
//                     questionById: newUnansweredQById
//                 }
//             }
//         default:
//             return state;
//     }
// }





// import { CohortReqActions } from "./actions";

// export interface CohortReqState {
//     cohortNum: number | string;
// }

// const initialState = {
//     cohortNum: "All",
// }

// export const cohortReqReducer = (state: CohortReqState = initialState, action: CohortReqActions): CohortReqState => {
//     switch (action.type) {
//         case "pickCohort":
//             return {
//                 cohortNum: action.cohortNum
//             }
//         default:
//             return state;
//     }
// }