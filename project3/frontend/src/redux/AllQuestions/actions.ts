
// action creator

import { ThunkDispatch, RootState } from "../../store"
import { push } from "connected-react-router"


export interface Question {
    id:               number;
    question_topic:   string;
    question_tag:     string;
    question_content: string;
    created_at:       string;
    updated_at:       string;
    creator_id:       number;
    likes_count: string | null;
    liked: string | null;
    cohort_source: string | number;
    views: number | null | 0;
    replies_count: string | null;
    replied: string | null;
}

export interface QCreator {
    userID: number;
    questionID: number;
    questionCreator: number;
}



export function loadedQuestions(questions: Question[]) {
    return {
        type: '@@question/LOADED_QUESTIONS' as '@@question/LOADED_QUESTIONS',
        questions
    }
}

export function loadedMostLikesQs(questions: Question[]) {
    return {
        type: '@@mostLikes/LOADED_MOST_LIKES_Q' as '@@mostLikes/LOADED_MOST_LIKES_Q',
        questions
    }
}

export function loadedMostViewsQs(questions: Question[]) {
    return {
        type: '@@mostViews/LOADED_MOSTVIEWS' as '@@mostViews/LOADED_MOSTVIEWS',
        questions
    }
}

export function loadedHottestQs(questions: Question[]) {
    return {
        type: '@@hottestQ/LOADED_HOTTEST_Q' as '@@hottestQ/LOADED_HOTTEST_Q',
        questions
    }
}


export function loadedAnsweredQs(questions: Question[]) {   
    return {
        type: '@@answeredQ/LOADED_ANSWERED_Q' as '@@answeredQ/LOADED_ANSWERED_Q',
        questions
    }
}

export function loadedUnansweredQs(questions: Question[]) {
    return {
        type: '@@unansweredQ/LOADED_UNANSWERED_Q' as '@@unansweredQ/LOADED_UNANSWERED_Q',
        questions
    }
}

export function pickCohort(cohortNum: number | string) {
    return {
        type: "pickCohort" as "pickCohort",
        cohortNum
    }
}



export type QuestionActions = ReturnType<typeof loadedQuestions | typeof loadedMostViewsQs | typeof loadedHottestQs | typeof loadedMostLikesQs | typeof loadedAnsweredQs | typeof loadedUnansweredQs | typeof pickCohort>

//try catch
//add function --token,401 checking
export function fetchQuestions() {
    return async (dispatch: ThunkDispatch, getState: ()=> RootState) => {
        try{
            const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/question/all`, {
                headers: {
                    'Authorization': `Bearer ${getState().auth.token}`
                }
            })
            const json = await res.json();
            // console.log(json)
            if(res.status === 200){
                dispatch(loadedQuestions(json));
            } else {
                dispatch(push('/login'))
            }
        } catch(e){
            console.log(e)
            //dispatch(logout())
        }  
    }
}

export function fetchMostLikesQs() {
    return async (dispatch: ThunkDispatch, getState: ()=> RootState) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/question/most_likes`, {
            headers: {
                'Authorization': `Bearer ${getState().auth.token}`
            }
        })
        const json = await res.json();
        // console.log(json)
        if(res.status === 200){
            dispatch(loadedMostLikesQs(json));
        } else {
            dispatch(push('/login'))
        }
    }
}

export function fetchMostViewsQs() {
    return async (dispatch: ThunkDispatch, getState: ()=> RootState) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/question/most_views`, {
            headers: {
                'Authorization': `Bearer ${getState().auth.token}`
            }
        })
        const json = await res.json();
        // console.log(json)
        if(res.status === 200){
            dispatch(loadedMostViewsQs(json));
        } else {
            dispatch(push('/login'))
        }
    }
}

export function fetchHottestQs() {
    return async (dispatch: ThunkDispatch, getState: ()=> RootState) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/question/hottest`,{
            headers: {
                'Authorization': `Bearer ${getState().auth.token}`
            }
        })
        const json = await res.json();
        // console.log(json)
        if(res.status === 200){
            dispatch(loadedHottestQs(json));
        } else {
            dispatch(push('/login'))
        }
    }
}


export function fetchAnsweredQs() {
    return async (dispatch: ThunkDispatch, getState: ()=> RootState) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/question/answered`, {
            headers: {
                'Authorization': `Bearer ${getState().auth.token}`
            }
        })
        const json = await res.json();
        // console.log(json)
        if(res.status === 200){
            dispatch(loadedAnsweredQs(json));
        } else {
            dispatch(push('/login'))
        }
    }
}

export function fetchUnansweredQs() {
    return async (dispatch: ThunkDispatch, getState: ()=> RootState) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/question/open`, {
            headers: {
                'Authorization': `Bearer ${getState().auth.token}`
            }
        })
        const json = await res.json();
        // console.log(json)
        if(res.status === 200){
            dispatch(loadedUnansweredQs(json));
        } else {
            dispatch(push('/login'))
        }
    }
}



// export function pickCohort(cohortNum: number | string) {
    //     return {
    //         type: "pickCohort" as "pickCohort",
    //         cohortNum
    //     }
    // }
    
    // export type CohortReqActions = ReturnType< typeof pickCohort>