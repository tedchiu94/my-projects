export interface RepliesObj {
    reply_id: number | null
    reply_content: string;
    replies_replier_id: number | null;
    reply_upload_file: string | null;
    reply_replier_nickname: string | null;
}

export interface Reply {
    question_id: number;
    question_topic: string;
    question_content: string;
    creator_id: number | null;
    replies: RepliesObj[]
}

export function loadedReplies(replies: Reply){
    return{
        type: '@@reply/LOADED_REPLY' as '@@reply/LOADED_REPLY',
        replies
    }
}

export type ReplyActions = ReturnType<typeof loadedReplies>