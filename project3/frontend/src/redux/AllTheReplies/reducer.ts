import { Reply, ReplyActions } from './actions';

export interface RepliesQuestionState {
    replies: Reply;
}

const initialRepliesState = {
    repliesObj: [{reply_id:0, reply_content:'no reply',replies_replier_id: null,reply_upload_file: null, reply_replier_nickname: null}]
}
const initialState = {
    replies: {
        question_id: 0,
        question_topic: 'no question',
        question_content: 'no question',
        creator_id: null,
        replies: (initialRepliesState.repliesObj)
    }
}

export const replyReducer = (state: RepliesQuestionState = initialState, action: ReplyActions): RepliesQuestionState => {
    switch (action.type) {
        case '@@reply/LOADED_REPLY':
            {
                const newReplies:Reply = action.replies;
                return {
                    replies: newReplies
                }
            }
        default:
            return state;
    }
}
