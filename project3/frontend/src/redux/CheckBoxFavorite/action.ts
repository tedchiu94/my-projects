
//action creator
export function checkedOrNot(arrFav:boolean[]) {
    return {
        type: 'checked' as 'checked',
        arrFav
    }
}

//action types
export type CheckedAction = ReturnType<typeof checkedOrNot>