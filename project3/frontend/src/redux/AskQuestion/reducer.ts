import { AskQActions } from "./actions"

export interface AskQState {
    askQ: string;
   
}

const initialState = {
    askQ: "no",
    
}
export const askQReducer = (state: AskQState = initialState, action: AskQActions): AskQState => {
    switch (action.type) {
        case "askQ":
            return {
                askQ: "yes",
            }

        case "closeAskQ":
            return {
                askQ: "no",
            }
        default:
            return state;
    }   
    
}

