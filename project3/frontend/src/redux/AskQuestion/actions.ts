export function askQ(){
    return {
        type: "askQ" as "askQ"
    }
}

export function closeAskQ(){
    return {
        type: "closeAskQ" as "closeAskQ" 
    }
}

export type AskQActions = ReturnType<typeof askQ | typeof closeAskQ>

