export function searchingText(text: string) {
    return {
        type: '@@searchText' as '@@searchText',
        text
    }
}

export type SearchTextAction = ReturnType<typeof searchingText>