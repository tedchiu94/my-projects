import { SearchTextAction } from "./action"

const initialState:string = ''

export const searchTextReducer = (state: string = initialState, action:SearchTextAction) => {
    switch (action.type) {
        case '@@searchText':
            {
                return action.text
            }
    default: 
            return state
    }
}