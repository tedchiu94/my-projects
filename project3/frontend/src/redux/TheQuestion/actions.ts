export function readQ(questionID: number){
    return {
        type: "readQ" as "readQ",
        questionID: questionID
    }
}


export function closeReadQ(){
    return {
        type: "closeReadQ" as "closeReadQ"
    }
}


export type ReadQActions = ReturnType<typeof readQ | typeof closeReadQ>


