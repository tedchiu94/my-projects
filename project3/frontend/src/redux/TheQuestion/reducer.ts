import { ReadQActions } from "./actions"

export interface ReadQState {
    questionID: number;
}

const initialState = {
    questionID: 0,
}



export const readQReducer = (state: ReadQState = initialState, action: ReadQActions): ReadQState => {
    switch (action.type) {
        //@@TheQusetion/READQ
        case "readQ":
            return {
                ...state,
                questionID: action.questionID,
            }
        //@@TheQusetion/CLOSEREADQ
        case "closeReadQ":
            return {
                ...state,
                questionID: 0,
            }
        default:
            return state;
    }

}
