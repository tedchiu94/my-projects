// Carl
import { UserService } from "../services/userService";
import express from 'express'
import { checkPassword } from "../hash";
import jwt from "../jwt";
import jwtSimple from 'jwt-simple';

export class AuthRouter {
    constructor(private userService: UserService) {

    }

    router() {
        const router = express.Router();
        router.post('/login', this.post);
        return router;
    }

    private post = async (req: express.Request, res: express.Response) => {
        try {
            if (!req.body.username || !req.body.password) {
                res.status(401).json({ msg: "Wrong Username/Password" });
                return;
            }
            const { username, password } = req.body;
            const user = (await this.userService.getUserByUsername(username));
            if (!user || !(await checkPassword(password, user.password))) {
                res.status(401).json({ msg: "Wrong Username/Password" });
                return;
            }
            const payload = {
                id: user.id,
                username: user.username,
            };
            const token = jwtSimple.encode(payload, jwt.jwtSecret);
            res.json({
                token: token
            });
        } catch (e) {
            console.log(e)
            res.status(500).json({ msg: e.toString() })
        }
    }
}
// Carl