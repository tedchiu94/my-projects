// Carl
import * as Knex from "knex";
import { hashPassword } from "../hash";

export async function seed(knex: Knex): Promise<void> {
  await knex('users').del()
  await knex("questions").del();
  await knex('replies').del();

  await knex('users').insert([
    { username: "Carl", password: await hashPassword('12345'), email: "hello@gmail.com" },
    { username: "Ted", password: await hashPassword('12345'), email: "hello@gmail.com" },
    { username: "Rex", password: await hashPassword('12345'), email: "hello@gmail.com" },
  ]).into('users')

  await knex("questions").insert([
    {
      question_topic: "What is React and what is its differences from html as a frontend?",
      question_content: "React is XXXXXXXXXXXX, html is XXXXXXXXXXXXXX",
      question_tag: ["react", "redux"],
      cohort_source: 10,
      
    },
    {
      question_topic: "What are the advantages using React?",
      question_content: "To apply React, you will benefit from ...XXXXXXXXXXX",
      question_tag: "react",
      cohort_source: 9,
    },
    {
      question_topic: "Programmatically navigate using react router?",
      question_content: "With react-router I can use the Link element to create links which are natively handled by react router.",
      question_tag: ["react", "css"],
      cohort_source: 9,
    },
    {
      question_topic: "What is the difference between React Native and React??",
      question_content: "I have started to learn React out of curiosity and wanted to know the difference between React and React Native - though could not find a satisfactory answer using Google..",
      question_tag: ["react", "redux"],
      cohort_source: 11,
    },
    {
      question_topic: "What do these three dots in React do?",
      question_content: " What does the ... do in this React (using JSX) code and what is it called? <Modal {...this.props} title='Modal heading' animation={false}>",
      question_tag: ["sql", "knex"],
      cohort_source: 12,
    },
    {
      question_topic: "What is the difference between state and props in React??",
      question_content: "I was watching a Pluralsight course on React and the instructor stated that props should not be changed.",
      question_tag: ["CICD", "AWS"],
      cohort_source: 13,
    },
    {
      question_topic: "What is the difference between state and props in React??",
      question_content: "React js onClick can't pass value to method",
      question_tag: ["python", "VScode"],
      cohort_source: 14,
    },
    {
      question_topic: "How do I conditionally add attributes to React components?",
      question_content: "Is there a way to only add attributes to a React component if a certain condition is met? I'm supposed to add required and readOnly attributes to form elements based on an Ajax call after render … identifier).",
      question_tag: ["login", "token"],
      cohort_source: 15,
    },
    {
      question_topic: "Hide keyboard in react-native",
      question_content: "this in all the tutorials and blog posts that I read. This basic example is still not working for me with react-native 0.4.2 in the Simulator. Couldn't try it on my iPhone yet.",
      question_tag: ["elastic search", "checkbox"],
      cohort_source: 16,
    },
  ]).returning('id');

  // await knex('replies').insert([
  //   {
  //     reply_content: "test1 reply_content",
  //     question_id: 1,
  //     replier_id: 1
  //   },
  //   {
  //     reply_content: "test2 reply_content",
  //     question_id: 1,
  //     replier_id: 2
  //   }
  // ])
};
// Carl
