import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('users', table => {
        table.integer('question_points');
        table.integer('reply_points');
        table.integer('rank');
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('users', table => {
        table.dropColumn('rank')
        table.dropColumn('reply_points')
        table.dropColumn('question_points')
    })
}