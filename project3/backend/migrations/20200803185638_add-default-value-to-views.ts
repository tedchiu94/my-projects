import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('questions', table => {
        table.integer('views').defaultTo(0).alter();
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('questions', table => {
        table.integer('views').defaultTo(null).alter()
    })
}