import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('replies', table => {
        table.increments();
        table.string('reply_content');
        table.integer('question_id').unsigned();
        table.foreign('question_id').references('questions.id');
        table.integer('replier_id').unsigned();
        table.foreign('replier_id').references('users.id');

    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable('replies');
}

