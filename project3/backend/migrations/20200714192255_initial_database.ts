// Carl
import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('users', (table) => {
        table.increments();
        table.string('username');
        table.string('password');
        table.string('email').unique();
        table.string('profilepic');
        table.timestamps(false, true);
    });
    // knex.schema.alterTable('users',table=>{
    //     table.string('email').unique().alter();
    // });
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable('users');
    // knex.schema.alterTable('users',table=>{
    //     table.dropUnique(['email']);
    // });
}
// Carl
