import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('replies', table => {
        table.string('reply_upload_file')
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('replies', table => {
        table.dropColumn('reply_upload_file')
    })
}

