import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('likes', table => {
        table.unique(['user_id','question_id'])
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('likes', table => {
        table.dropUnique(['user_id','question_id'])
    })
}

