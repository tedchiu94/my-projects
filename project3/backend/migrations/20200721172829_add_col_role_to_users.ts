import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('users', table => {
        table.string('role')
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('users', table => {
        table.dropColumn('role')
    })
}

