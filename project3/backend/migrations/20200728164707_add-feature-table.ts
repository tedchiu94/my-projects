import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('likes', table => {
        table.integer('user_id').unsigned();
        table.foreign('user_id').references('users.id');
        table.integer('question_id').unsigned();
        table.foreign('question_id').references('questions.id');
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable('likes')
}

