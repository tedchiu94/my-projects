import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('likes', table => {
        table.integer('question_creator');
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('likes', table => {
        table.dropColumn('question_creator')
    })
}