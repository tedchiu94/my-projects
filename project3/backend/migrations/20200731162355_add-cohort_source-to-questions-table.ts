import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('questions', table => {
        table.integer('cohort_source');
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('questions', table => {
        table.dropColumn('cohort_source')

    })
}