// Carl
import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('user_question_favorite', (table) => {
        table.increments();
        table.integer('user_id').unsigned();
        table.foreign('user_id').references('users.id');
        table.integer('question_id').unsigned();
        table.foreign('question_id').references('questions.id');
        table.boolean('favorite');
        table.timestamps(false, true);
    });
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable('user_question_favorite');
}
// Carl