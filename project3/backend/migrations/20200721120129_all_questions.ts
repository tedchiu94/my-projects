import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('questions', table => {
        table.increments();
        table.string('question_topic');
        table.string('question_content');
        table.string('question_tag')
        table.timestamps(false, true);

    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable('questions');
}

