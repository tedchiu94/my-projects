import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('questions', table => {
        table.integer('creator_id').unsigned();
        table.foreign('creator_id').references('users.id');
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('questions', table => {
        table.dropColumn('creator_id');
    })
}

