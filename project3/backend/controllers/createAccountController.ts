import { Request, Response } from 'express';
import { CreateAccountService, UserData, ProfilePic } from '../services/createAccountService';
import xlsx from 'xlsx';

interface PresetUser {
    username: string,
    email: string,
    role: string
}

export class CreateAccountController {

    constructor(private createAccountService: CreateAccountService) { }

    public addNewUserData = async (req: Request, res: Response) => {
        try {
            // console.log(req.body);
            // console.log((req.file as any)?.key);
            const userData: UserData = req.body;
            const profilePicData: ProfilePic = (req.file as any);
            const file = xlsx.readFile('./tutorStudentData.xlsx');
            const presetUsers: PresetUser[] = xlsx.utils.sheet_to_json(file.Sheets['users']);
            if (userData.role === 'tutor') {
                for (let presetUser of presetUsers) {
                    if (presetUser.username === userData.username && presetUser.email === userData.email) {
                        await this.createAccountService.addNewUserData(userData, profilePicData);
                        res.json({ success: true });
                    }
                }
            } else if (userData.role === 'student') {
                for (let presetUser of presetUsers) {
                    if (presetUser.username === userData.username && presetUser.email === userData.email) {
                        await this.createAccountService.addNewUserData(userData, profilePicData);
                        res.json({ success: true });
                    }
                }
            } else {
                res.status(401).json({ msg: 'wrong role' });
            }
        }
        catch (err) {
            console.error(err);
            res.status(500).json({ message: 'internal server error' });
        }
    }
}

// app.post('/userac', upload.single('photo'), async (req, res) => {
//     console.log(req.body)
//     const file = xlsx.readFile('./tutorStudentData.xlsx');
//     const presetUsers: PresetUser[] = xlsx.utils.sheet_to_json(file.Sheets['users']);
//     if (req.body.role === 'tutor') {
//         for (let presetUser of presetUsers) {
//             if (presetUser.username === req.body.username && presetUser.email === req.body.email) {
//                 await knex('users').insert([
//                     { username: req.body.username, password: await hashPassword(req.body.password), email: req.body.email, profilepic: req.file.filename, role: req.body.role },
//                 ]).into('users');
//                 res.json({ success: true })
//             }
//         }
//     } else if (req.body.role === 'student') {
//         for (let presetUser of presetUsers) {
//             if (presetUser.username === req.body.username && presetUser.email === req.body.email) {
//                 await knex('users').insert([
//                     { username: req.body.username, password: await hashPassword(req.body.password), email: req.body.email, profilepic: req.file.filename, role: req.body.role },
//                 ]).into('users');
//                 res.json({ success: true })
//             }
//         }
//     } else {
//         res.status(401).json({ msg: 'wrong role' })
//     }
// })