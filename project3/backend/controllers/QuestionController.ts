import express from 'express';
import Knex from 'knex';


export class QuestionController {
    public constructor(private knex: Knex) {}

    public getAllQuestions = async (req: express.Request, res: express.Response) => {
    const rows = await this.knex.raw(/*sql*/`
        SELECT questions.*, 
            (SELECT COUNT(likes.question_id) FILTER (WHERE likes.user_id = ? AND likes.question_id = questions.id) FROM likes) AS liked,
            (SELECT COUNT(likes.question_id) FROM likes WHERE likes.question_id = questions.id) AS likes_count,
            (SELECT COUNT(replies.question_id) FILTER (WHERE replies.replier_id = ? AND replies.question_id = questions.id) FROM replies) AS replied,
            (SELECT COUNT(replies.question_id) FROM replies WHERE replies.question_id = questions.id) AS replies_count
        FROM questions
        GROUP BY questions.id 
        ORDER BY questions.id DESC`, [req.user?.id || 0, req.user?.id || 0]);
        // console.log(rows.rows)
        res.json(rows.rows);
    }

    
    public getMostLikedQuestions = async (req: express.Request, res: express.Response) => {
        const rows = await this.knex.raw(/*sql*/`
        SELECT questions.*, 
            (SELECT COUNT(likes.question_id) FILTER (WHERE likes.user_id = ? AND likes.question_id = questions.id) FROM likes) AS liked,
            (SELECT COUNT(likes.question_id) FROM likes WHERE likes.question_id = questions.id) AS likes_count,
            (SELECT COUNT(replies.question_id) FILTER (WHERE replies.replier_id = ? AND replies.question_id = questions.id) FROM replies) AS replied,
            (SELECT COUNT(replies.question_id) FROM replies WHERE replies.question_id = questions.id) AS replies_count
        FROM questions
        GROUP BY questions.id 
        HAVING ((SELECT COUNT(likes.question_id) FROM likes WHERE likes.question_id = questions.id) > 0)
        ORDER BY likes_count DESC, questions.id DESC`, [req.user?.id || 0, req.user?.id || 0]);
        res.json(rows.rows);
    }


    public getMostViewsQuestions = async (req: express.Request, res: express.Response) => {
        const rows = await this.knex.raw(/*sql*/`
        SELECT questions.*, 
            (SELECT COUNT(likes.question_id) FILTER (WHERE likes.user_id = ? AND likes.question_id = questions.id) FROM likes) AS liked,
            (SELECT COUNT(likes.question_id) FROM likes WHERE likes.question_id = questions.id) AS likes_count,
            (SELECT COUNT(replies.question_id) FILTER (WHERE replies.replier_id = ? AND replies.question_id = questions.id) FROM replies) AS replied,
            (SELECT COUNT(replies.question_id) FROM replies WHERE replies.question_id = questions.id) AS replies_count
        FROM questions 
        GROUP BY questions.id
        HAVING (questions.views > 0)
        ORDER BY questions.views DESC, questions.id DESC`, [req.user?.id || 0, req.user?.id || 0]);
        res.json(rows.rows);
    }


    public getHottestQuestions = async (req: express.Request, res: express.Response) => {
        const rows = await this.knex.raw(/*sql*/`
        SELECT questions.*, 
            (SELECT COUNT(likes.question_id) FILTER (WHERE likes.user_id = ? AND likes.question_id = questions.id) FROM likes) AS liked,
            (SELECT COUNT(likes.question_id) FROM likes WHERE likes.question_id = questions.id) AS likes_count,
            (SELECT COUNT(replies.question_id) FILTER (WHERE replies.replier_id = ? AND replies.question_id = questions.id) FROM replies) AS replied,
            (SELECT COUNT(replies.question_id) FROM replies WHERE replies.question_id = questions.id) AS replies_count
        FROM questions 
        GROUP BY questions.id
        HAVING ((SELECT COUNT(replies.question_id) FROM replies WHERE replies.question_id = questions.id) > 0)
        ORDER BY replies_count DESC, questions.id DESC`, [req.user?.id || 0 , req.user?.id || 0]);
        res.json(rows.rows);
    }


    public getAnsweredQuestions = async (req: express.Request, res: express.Response) => {
        // let userId = req.user?.id || 0;
        const rows = await this.knex.raw(/*sql*/`
        SELECT questions.*, 
            (SELECT COUNT(likes.question_id) FILTER (WHERE likes.user_id = ? AND likes.question_id = questions.id) FROM likes) AS liked,
            (SELECT COUNT(likes.question_id) FROM likes WHERE likes.question_id = questions.id) AS likes_count,
            (SELECT COUNT(replies.question_id) FILTER (WHERE replies.replier_id = ? AND replies.question_id = questions.id) FROM replies) AS replied,
            (SELECT COUNT(replies.question_id) FROM replies WHERE replies.question_id = questions.id) AS replies_count
        FROM questions 
        GROUP BY questions.id
        HAVING ((SELECT COUNT(replies.question_id) FROM replies WHERE replies.question_id = questions.id) > 0)
        ORDER BY questions.id DESC`, [req.user?.id || 0 , req.user?.id || 0]);
        res.json(rows.rows);
    }

    
    public getUnansweredQuestions = async (req: express.Request, res: express.Response) => {
        const rows = await this.knex.raw(/*sql*/`
        SELECT questions.*, 
            (SELECT COUNT(likes.question_id) FILTER (WHERE likes.user_id = ? AND likes.question_id = questions.id) FROM likes) AS liked,
            (SELECT COUNT(likes.question_id) FROM likes WHERE likes.question_id = questions.id) AS likes_count,
            (SELECT COUNT(replies.question_id) FILTER (WHERE replies.replier_id = ? AND replies.question_id = questions.id) FROM replies) AS replied,
            (SELECT COUNT(replies.question_id) FROM replies WHERE replies.question_id = questions.id) AS replies_count
        FROM questions 
        GROUP BY questions.id
        HAVING ((SELECT COUNT(replies.question_id) FROM replies WHERE replies.question_id = questions.id) = 0)
        ORDER BY questions.id DESC`, [req.user?.id || 0 , req.user?.id || 0]);
        res.json(rows.rows);
    }

    public createQuestion = async (req: express.Request, res: express.Response) => {
        await this.knex('questions').insert([
            {
                question_topic: req.body.form.question_topic, question_tag: req.body.form.question_tag,
                question_content: req.body.form.question_content, creator_id: req.body.id
            },
        ]).into('questions');
        res.json({ success: true })
    }


    public likeQuestion = async (req: express.Request, res: express.Response) => {
        await this.knex.raw(/*sql*/`
        INSERT INTO likes (user_id, question_id) VALUES (? , ?);
        `, [req.user?.id!, req.params.id]);

        await this.knex.raw(/*sql*/`
        UPDATE users
        SET question_points = question_points + 1
        WHERE users.id IN (
            SELECT creator_id FROM questions
            WHERE questions.id = ?
        )`, [req.params.id]);

        res.json({ success: true })
    }


    public unlikeQuestion = async (req: express.Request, res: express.Response) => {
        await this.knex.raw(/*sql*/`
        DELETE FROM likes WHERE user_id = ? AND question_id = ?
        `, [req.user?.id!, req.params.id]);

        await this.knex.raw(/*sql*/`
        UPDATE users
        SET question_points = question_points - 1
        WHERE users.id IN (
            SELECT creator_id FROM questions
            WHERE questions.id = ?
        )`, [req.params.id]);

        res.json({ success: true })
    }


    public addViews = async (req: express.Request, res: express.Response) => {
        await this.knex.raw(/*sql*/`
        UPDATE questions
        SET views = views + 1 WHERE questions.id =  ?
        `, [req.params.id])

        res.json({ success: true });
    }

}

