import { Request, Response } from 'express';
import { CheckBoxService } from '../services/checkBoxService';

export interface CheckBoxData {
    user_id: number,
    question_id: number,
    checked: boolean
} 

export interface getCheckBoxData {
    user_id: number
}

export class CheckBoxController {

    constructor(private checkBoxService: CheckBoxService) {}

    public addOrUpdateUserQuestionFavData = async (req: Request, res: Response) => {
        try {
            const data:CheckBoxData = req.body;
            const userQuestionFavDatas = await this.checkBoxService.getUserQuestionFavData();
            for (let userQuestionFavData of userQuestionFavDatas) {
                if (userQuestionFavData.user_id == data.user_id && userQuestionFavData.question_id == data.question_id) {
                    await this.checkBoxService.updateUserQuestionFavData(data);
                    res.json({ success: true });
                    return;
                }
            }
            await this.checkBoxService.addUserQuestionFavData(data);
            res.json({ success: true });
        } 
        catch (err) {
            console.error(err);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    public getCheckBoxData = async (req: Request, res: Response) => {
        try {
            if (req.body.user_id) {
                const data:getCheckBoxData = req.body;
                const favQuestionIdDatas = await this.checkBoxService.getFavQuestionID(data);

                let arrCheckedQuestionID: number[] = [];
                for (let data of favQuestionIdDatas) {
                    arrCheckedQuestionID.push(data.question_id) // =arr of question_id that are true in table user_question_favorite
                }

                const dataQuestions = await this.checkBoxService.getTotalQuestionCount();

                let lengthOfQ = dataQuestions[0].count; //=total num of questions in table questions
                let arrOfFavQ: boolean[] = [];
                for (let i = 0; i < lengthOfQ; i++) {
                    arrOfFavQ[i] = false;
                }
                for (let checkedQ of arrCheckedQuestionID) {
                    arrOfFavQ[checkedQ - 1] = true;
                }
                res.json(arrOfFavQ.reverse())
            }
        }
        catch (err) {
            console.error(err);
            res.status(500).json({ message: 'internal server error' });
        }
    }
}



// app.post('/getcheckedbox', async (req, res) => {
//     if (req.body.user_id) {
//         const datas = await knex.raw(/*sql*/`SELECT question_id from user_question_favorite 
//         WHERE user_id = ? AND favorite = ?`,[req.body.user_id,true]);
//         //console.log(datas.rows);

//         let arrCheckedQuestionID: number[] = [];
//         for (let data of datas.rows) {
//             arrCheckedQuestionID.push(data.question_id) // =arr of question_id that are true in table user_question_favorite
//         }

//         const dataQuestions = await knex.raw(/*sql*/`SELECT COUNT(*) FROM questions`);
//         //console.log(dataQuestions.rows[0].count);

//         let lengthOfQ = dataQuestions.rows[0].count; //=total num of questions in table questions
//         let arrOfFavQ: boolean[] = [];
//         for (let i = 0; i < lengthOfQ; i++) {
//             arrOfFavQ[i] = false;
//         }

//         for (let checkedQ of arrCheckedQuestionID) {
//             arrOfFavQ[checkedQ - 1] = true;
//         }
//         //console.log(arrOfFavQ)
//         res.json(arrOfFavQ)
//     }
// })


// From main.ts b4 MVC refractoring
// app.post('/checkedbox', async (req, res) => {
//     // console.log(req.body);
//     const datas = await knex.raw(/*sql*/`SELECT * from user_question_favorite`);
//     // console.log(data.rows)
//     for (let data of datas.rows) {
//         if (data.user_id == req.body.user_id && data.question_id == req.body.question_id) {
//             await knex.raw(/*sql*/`UPDATE user_question_favorite SET favorite = ${req.body.checked} 
//             WHERE user_id = ${data.user_id} AND question_id = ${data.question_id}`)
//             res.json({ success: true })
//             return
//         }
//     }
//     await knex.insert([
//         { user_id: req.body.user_id, question_id: req.body.question_id, favorite: req.body.checked },
//     ]).into('user_question_favorite');
//     res.json({ success: true })
// })