import {Request, Response} from 'express';
import {SearchQuestionService} from '../services/searchQuestionSercive';

export class SearchQuestionController {

    constructor(private searchQuestionService: SearchQuestionService) {}

    public getSearchQuestion = async (req:Request, res:Response) => {
        try {
            const text = req.query.q;
            const searchQuestionResult = await this.searchQuestionService.getSearchQuestion(text);
            res.json(searchQuestionResult);
        } catch (err) {
            console.error(err);
            res.status(500).json({ message: 'internal server error'});
        }
    }
}

//Carl getSearchQ
// app.get('/searchQ', async (req, res) => {
//     const rows =  await knex.raw(/*sql*/`
//     SELECT questions.* FROM questions
//     WHERE questions.question_topic ILIKE ?
//     OR questions.question_content ILIKE ? 
//     `, ['%'+req.query.q+'%', '%'+req.query.q+'%']);
//     // console.log(rows.rows)
//     res.json(rows.rows)
// })
//Carl getSearchQ