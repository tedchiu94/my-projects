import { Request, Response } from 'express';
import { RepliesService } from '../services/repliesService';

export class RepliesController {

    constructor(private repliesService: RepliesService) { }

    public getReplies = async (req: Request, res: Response) => {
        try {
            const rows = await this.repliesService.getReplies();

            const rowsSecond = await this.repliesService.getUsername();

            let result = {};
            for (let item of rows.rows) {
                const { reply_id, reply_content, replies_replier_id, reply_upload_file } = item;
                const reply = { reply_id, reply_content, replies_replier_id, reply_upload_file };
                for (let item2 of rowsSecond.rows){
                    if(item2.user_id == reply.replies_replier_id){
                        reply["reply_replier_nickname"] = item2.user_nickname
                    }
                }
                if (result[item.question_id]) {
                    result[item.question_id].replies.push(reply);
                } else {
                    result[item.question_id] = {
                        question_id: item.question_id,
                        question_topic: item.question_topic,
                        question_content: item.question_content,
                        creator_id: item.question_creator_id,
                        replies: [reply]
                    };
                };
            };
            res.json(result);

        } catch (error) {
            console.error(error);
            res.status(500).json({ message: 'internal server error' });
        };

    };

    public postReply = async (req: Request, res: Response) => {
        try {
            const userID = parseInt(req.body.userId);
            const questionID = parseInt(req.body.questionId);
            const reply_content = req.body?.reply_content;
            // const reply_upload_file = req.file?.filename
            const reply_upload_file= (req.file as any)?.key
            await this.repliesService.postReply(reply_content, userID, questionID, reply_upload_file);
            res.json({ success: true })

        } catch (error) {
            console.error(error);
            res.status(500).json({ message: 'internal server error' });
        };
    };

}