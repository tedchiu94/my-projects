import Knex from 'knex';
import { getCheckBoxData } from '../controllers/checkBoxController';
import { hashPassword } from '../hash';

interface UserInfoUpdate {
    form: {nickname:string, password:string};
    id: number
}

export class ProfileService {

    constructor(private knex: Knex) {
    }

    public getFavQuestionID = async (data: number) => {
        const datas = await this.knex.raw(/*sql*/`SELECT question_id from user_question_favorite 
            WHERE user_id = ? AND favorite = ?`, [data, true]);
        return datas.rows;
    }

    public getRestructuredFavData = async (data: number[]) => {
        const datas = await this.knex.raw(/*sql*/`
                SELECT questions.id as question_id,
                questions.question_topic as question_topic,
                questions.question_content as question_content,
                questions.creator_id as question_creator_id,
                replies.id as reply_id,
                replies.reply_content as reply_content,
                replies.replier_id as replies_replier_id FROM questions left outer join replies on questions.id = replies.question_id
                WHERE questions.id = ANY(?)
               `, [data]);
        return datas.rows;
    }

    public getUserRecordCheckedbox = async (data:getCheckBoxData) => {
        const datas = await this.knex.raw(/*sql*/`SELECT question_id from user_question_favorite 
                 WHERE user_id = ? AND favorite = ?`,[data.user_id,true]);
        return datas.rows;
    }

    public getPostedQuestions = async (data: number) => {
        const datas = await this.knex.raw(/*sql*/`
                SELECT questions.id as question_id,
                questions.question_topic as question_topic,
                questions.question_content as question_content FROM questions 
                WHERE questions.creator_id = ?
                `, [data]);
        return datas.rows;
    }

    public amendUserAccount = async (data:UserInfoUpdate) => {
        await this.knex.raw(/*sql*/`UPDATE users SET (nickname, password) = (?, ?) 
            WHERE id = ?`, 
            [data.form.nickname, await hashPassword(data.form.password), +data.id])
    }
}

// (4)
// app.post('/AmendUserAC', async (req, res) => {
//     console.log(req.body);
//     await knex.raw(/*sql*/`UPDATE users SET (nickname, password) = (?, ?) 
//              WHERE id = ?`, 
//              [req.body.form.nickname, await hashPassword(req.body.form.password), req.body.id])
//     res.json({success: true})
// })


// (3)
// app.get('/PostedQuestions', async (req, res) => {
//     if (req.query.q) {
//         const rows = await knex.raw(/*sql*/`
//             SELECT questions.id as question_id,
//             questions.question_topic as question_topic,
//             questions.question_content as question_content FROM questions 
//             WHERE questions.creator_id = ?
//             `, [+req.query.q]);
//         // console.log('Testing PostedQ'+rows.rows);
//         res.json(rows.rows);
//     }
// })

// (2)
// app.post('/getUserRecordCheckedbox', async (req, res) => {
//     if (req.body.user_id) {
//         const datas = await knex.raw(/*sql*/`SELECT question_id from user_question_favorite 
//         WHERE user_id = ? AND favorite = ?`,[req.body.user_id,true]);
//         //console.log(datas.rows);

//         let arrCheckedQuestionID: number[] = [];
//         for (let data of datas.rows) {
//             arrCheckedQuestionID.push(data.question_id) // =arr of question_id that are true in table user_question_favorite
//         }

//         let lengthOfQ = arrCheckedQuestionID.length; //=total num of questions in arrCheckedQuestionID
//         let arrOfFavQ: boolean[] = [];
//         for (let i = 0; i < lengthOfQ; i++) {
//             arrOfFavQ[i] = true;
//         }

//         res.json(arrOfFavQ);
//     }
// })


// (1)
// app.get('/FavQuestions', async (req, res) => {
//     // console.log(`this is:${req.query.q}`);
//     if (req.query.q) {
//        const rows =  await knex.raw(/*sql*/`SELECT question_id from user_question_favorite 
//         WHERE user_id = ? AND favorite = ?`,[+req.query.q,true]);
//     //    console.log(rows.rows)

//        let questionsArr = [];
//        for (let item of rows.rows) {
//            questionsArr.push(item.question_id);
//        }
//     //    console.log(questionsArr.sort());

//        const dataFav = await knex.raw(/*sql*/`
//         SELECT questions.id as question_id,
//         questions.question_topic as question_topic,
//         questions.question_content as question_content,
//         questions.creator_id as question_creator_id,
//         replies.id as reply_id,
//         replies.reply_content as reply_content,
//         replies.replier_id as replies_replier_id FROM questions left outer join replies on questions.id = replies.question_id
//         WHERE questions.id = ANY(?)
//        `, [questionsArr.sort()]);
//     //    console.log(dataFav.rows);

//        let result = {};
//        for (let item of dataFav.rows) {
//            const { reply_id, reply_content, replies_replier_id } = item;
//            const reply = { reply_id, reply_content, replies_replier_id };
//            if (result[item.question_id]) {
//                result[item.question_id].replies.push(reply);
//            } else {
//                result[item.question_id] = {
//                    question_id: item.question_id,
//                    question_topic: item.question_topic,
//                    question_content: item.question_content,
//                    creator_id: item.question_creator_id,
//                    replies: [reply]
//                };
//            }
//        }
//     //    console.log(result);
//        res.json(result);
//     }
// })