import Knex from 'knex';
import { CheckBoxData, getCheckBoxData } from '../controllers/checkBoxController';

export class CheckBoxService {

    constructor(private knex: Knex) {
    }

    public getUserQuestionFavData = async () => {
        const datas = await this.knex.raw(/*sql*/`SELECT * from user_question_favorite`);
        // console.log(data.rows);
        return datas.rows
    }

    public updateUserQuestionFavData = async (data:CheckBoxData) => {
        await this.knex.raw(/*sql*/`UPDATE user_question_favorite SET favorite = ${data.checked} 
             WHERE user_id = ? AND question_id = ?`, 
             [data.user_id,data.question_id])
    }

    public addUserQuestionFavData = async (data:CheckBoxData) => {
        await this.knex.insert([
            { user_id: data.user_id, question_id: data.question_id, favorite: data.checked },
        ]).into('user_question_favorite');
    }

    // for main.ts /getcheckedbox
    public getFavQuestionID = async (data:getCheckBoxData) => {
        const datas = await this.knex.raw(/*sql*/`SELECT question_id from user_question_favorite 
            WHERE user_id = ? AND favorite = ?`,[data.user_id,true]);
        return datas.rows
    }

    public getTotalQuestionCount = async () => {
        const datas = await this.knex.raw(/*sql*/`SELECT COUNT(*) FROM questions`);
        return datas.rows
    }
}


// app.post('/getcheckedbox', async (req, res) => {
//     if (req.body.user_id) {
//         const datas = await knex.raw(/*sql*/`SELECT question_id from user_question_favorite 
//         WHERE user_id = ? AND favorite = ?`,[req.body.user_id,true]);
//         //console.log(datas.rows);

//         let arrCheckedQuestionID: number[] = [];
//         for (let data of datas.rows) {
//             arrCheckedQuestionID.push(data.question_id) // =arr of question_id that are true in table user_question_favorite
//         }

//         const dataQuestions = await knex.raw(/*sql*/`SELECT COUNT(*) FROM questions`);
//         //console.log(dataQuestions.rows[0].count);

//         let lengthOfQ = dataQuestions.rows[0].count; //=total num of questions in table questions
//         let arrOfFavQ: boolean[] = [];
//         for (let i = 0; i < lengthOfQ; i++) {
//             arrOfFavQ[i] = false;
//         }

//         for (let checkedQ of arrCheckedQuestionID) {
//             arrOfFavQ[checkedQ - 1] = true;
//         }
//         //console.log(arrOfFavQ)
//         res.json(arrOfFavQ)
//     }
// })








// app.post('/checkedbox', async (req, res) => {
//     // console.log(req.body);
//     const datas = await knex.raw(/*sql*/`SELECT * from user_question_favorite`);
//     // console.log(data.rows)
//     for (let data of datas.rows) {
//         if (data.user_id == req.body.user_id && data.question_id == req.body.question_id) {
//             await knex.raw(/*sql*/`UPDATE user_question_favorite SET favorite = ${req.body.checked} 
//             WHERE user_id = ${data.user_id} AND question_id = ${data.question_id}`)
//             res.json({ success: true })
//             return
//         }
//     }
//     await knex.insert([
//         { user_id: req.body.user_id, question_id: req.body.question_id, favorite: req.body.checked },
//     ]).into('user_question_favorite');
//     res.json({ success: true })
// })