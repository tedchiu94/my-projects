import Knex from 'knex';

export class RepliesService {
    constructor(private knex: Knex) { };

    async getReplies() {
        const rows = await this.knex.raw(/*sql*/`
            SELECT questions.id as question_id,
            questions.question_topic as question_topic,
            questions.question_content as question_content,
            questions.creator_id as question_creator_id,
            replies.id as reply_id,
            replies.reply_content as reply_content,
            replies.reply_upload_file as reply_upload_file,
            replies.replier_id as replies_replier_id FROM questions left outer join replies on questions.id = replies.question_id
        `);
        
        return rows;
    };

    async getUsername() {
        const rows = await this.knex.raw(`
            SELECT users.id as user_id,
            users.nickname as user_nickname FROM users
        `)
        return rows;
    }

    async postReply(reply_content:any,userID:any,questionID:any,reply_upload_file:any) {
        await this.knex('replies')
         .insert([
             {
                 reply_content: reply_content,
                 question_id: questionID,
                 replier_id: userID,
                 reply_upload_file: reply_upload_file
             }
         ])
    }
}