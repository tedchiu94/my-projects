import Knex from 'knex';
import { hashPassword } from '../hash';

export interface UserData {
    username:string;
    password:string;
    email:string;
    role:string;
    nickname:string
}
 
export interface ProfilePic {
    key:string
}

export class CreateAccountService {

    constructor(private knex: Knex) {
    }

    public addNewUserData = async (userData:UserData, profilePicData:ProfilePic) => {
        await this.knex.insert([
            { username: userData.username, password: await hashPassword(userData.password), 
                email: userData.email, profilepic: profilePicData?.key, role: userData.role, nickname: userData.nickname},
            ]).into('users');

        // await this.knex.transaction(async (trx) => {
        //     try{
        //         await trx.insert([
        //             { username: userData.username, password: await hashPassword(userData.password), 
        //                 email: userData.email, profilepic: profilePicData?.key, role: userData.role, nickname: userData.nickname},
        //             ]).into('users');
        //     } catch(err) {
        //         console.error(err)
        //     }
        // })
    }

}


// app.post('/userac', upload.single('photo'), async (req, res) => {
//     console.log(req.body)
//     const file = xlsx.readFile('./tutorStudentData.xlsx');
//     const presetUsers: PresetUser[] = xlsx.utils.sheet_to_json(file.Sheets['users']);
//     if (req.body.role === 'tutor') {
//         for (let presetUser of presetUsers) {
//             if (presetUser.username === req.body.username && presetUser.email === req.body.email) {
//                 await knex('users').insert([
//                     { username: req.body.username, password: await hashPassword(req.body.password), email: req.body.email, profilepic: req.file.filename, role: req.body.role },
//                 ]).into('users');
//                 res.json({ success: true })
//             }
//         }
//     } else if (req.body.role === 'student') {
//         for (let presetUser of presetUsers) {
//             if (presetUser.username === req.body.username && presetUser.email === req.body.email) {
//                 await knex('users').insert([
//                     { username: req.body.username, password: await hashPassword(req.body.password), email: req.body.email, profilepic: req.file.filename, role: req.body.role },
//                 ]).into('users');
//                 res.json({ success: true })
//             }
//         }
//     } else {
//         res.status(401).json({ msg: 'wrong role' })
//     }
// })