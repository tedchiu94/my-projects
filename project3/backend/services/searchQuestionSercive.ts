import Knex from 'knex';

export class SearchQuestionService {

    constructor(private knex: Knex) {
    }

    public getSearchQuestion = async (text:any) => {
        const rows = await this.knex.raw(/*sql*/`
        SELECT questions.* FROM questions
        WHERE questions.question_topic ILIKE ?
        OR questions.question_content ILIKE ? 
        `, ['%' + text + '%', '%' + text + '%']);

        return rows.rows;
    }
}


// import {SearchQuestionService} from './services/searchQuestionSercive';
// import {SearchQuestionController} from './controllers/searchQuestionController';

// const searchQuestionService = new SearchQuestionService(knex);
// export const searchQuestionController = new SearchQuestionController(searchQuestionService);

// import {routes} from './routes';
// app.use('/', routes);


// From main.ts b4 refractoring
// app.get('/searchQ', async (req, res) => {
//     const rows =  await knex.raw(/*sql*/`
//     SELECT questions.* FROM questions
//     WHERE questions.question_topic ILIKE ?
//     OR questions.question_content ILIKE ? 
//     `, ['%'+req.query.q+'%', '%'+req.query.q+'%']);
//     // console.log(rows.rows)
//     res.json(rows.rows)
// })