// Carl
import Knex from "knex";

export interface User {
    id: number;
    username: string;
    password: string;
    email: string;
    profilepic: string;
    nickname: string;
}

export class UserService {
    constructor(private knex:Knex) {
    }

    public getUser = async (id:number): Promise<User> => {
        const result = await this.knex.raw(/*SQL*/`SELECT * FROM users WHERE id = ?`, [id]);
        return result.rows[0]
    }

    public getUserByUsername = async (username:string): Promise<User> => {
        const result = await this.knex.raw(/*SQL*/`SELECT * FROM users WHERE username = ?`, [username]);
        return result.rows[0]
    }
}
// Carl