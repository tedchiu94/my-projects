import express from 'express';
import {searchQuestionController, repliesController} from './main'
import { checkBoxController, profileController, upload, createAccountController} from './main';

export const routes = express.Router();


routes.get('/searchQ', searchQuestionController.getSearchQuestion);
routes.post('/checkedbox', checkBoxController.addOrUpdateUserQuestionFavData);
routes.post('/getcheckedbox', checkBoxController.getCheckBoxData);

routes.get('/FavQuestions', profileController.getFavQuestion);
routes.post('/getUserRecordCheckedbox', profileController.getUserRecordCheckedbox);
routes.get('/PostedQuestions', profileController.getPostedQuestions);
routes.post('/AmendUserAC', profileController.amendUserAccount);

routes.post('/userac', upload.single('photo'), createAccountController.addNewUserData);

routes.get('/replies', repliesController.getReplies);
routes.post('/replies', upload.single('photo'), repliesController.postReply);