import express from 'express';
import cors from 'cors';
// import dotenv from 'dotenv';
import Knex from 'knex';
//Carl
import bodyParser from 'body-parser';
import multer from 'multer';
import { UserService, User } from './services/userService';
import { AuthRouter } from './routers/authRouter';
import { UserRouter } from './routers/userRouter';
import { isLoggedIn } from './guard';

import aws from 'aws-sdk';
import multerS3 from 'multer-s3'

// import { hashPassword } from './hash';
// import xlsx from 'xlsx';
//Carl

import { QuestionController } from './controllers/QuestionController';

const app = express();

app.use(cors({
    origin: [
        process.env.REACT_DOMAIN!,
        process.env.REACT_DOMAIN2!,
        process.env.REACT_DOMAIN3!
    ]
}))

//Carl createUserAc + Multer 30-47
const s3 = new aws.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: 'ap-southeast-1'
});

export const upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: process.env.AWS_S3_BUCKET!,
        metadata: (req,file,cb)=>{
            cb(null,{fieldName: file.fieldname});
        },
        key: (req,file,cb)=>{
            cb(null,`${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
        }
    })
})

// const storage = multer.diskStorage({
//     destination: function (req, file, cb) {
//       cb(null, `${__dirname}/uploads`);
//     },
//     filename: function (req, file, cb) {
//       cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
//     }
//   })
// export const upload = multer({storage})

// app.use('/uploads', express.static('uploads'))
//Carl createUserAc + Multer

//Carl
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
//Carl

const knexConfig = require('./knexfile')
const knex = Knex(knexConfig[process.env.NODE_ENV || 'development'])
//Carl JWT 
declare global {
    namespace Express {
        interface Request {
            user?: User
        }
    }
}

const userService = new UserService(knex);
const authRouter = new AuthRouter(userService);
const userRouter = new UserRouter();
const questionController = new QuestionController(knex);

const isLoginGuard = isLoggedIn(userService);

app.use('/auth', authRouter.router());
app.use('/user', isLoginGuard, userRouter.router());
//Carl JWT 


// dotenv.config();



app.put('/question/:id/addviews', questionController.addViews)
app.get('/question/all', isLoginGuard, questionController.getAllQuestions)
app.post('/question/ask', questionController.createQuestion)
app.put('/question/:id/like' , isLoginGuard, questionController.likeQuestion)
app.put('/question/:id/unlike', isLoginGuard, questionController.unlikeQuestion)
app.get('/question/most_likes', isLoginGuard, questionController.getMostLikedQuestions)
app.get('/question/most_views', isLoginGuard, questionController.getMostViewsQuestions)
app.get('/question/hottest', isLoginGuard, questionController.getHottestQuestions)
app.get('/question/answered', isLoginGuard, questionController.getAnsweredQuestions)
app.get('/question/open', isLoginGuard, questionController.getUnansweredQuestions)

// ted - get replies(answer) from database
// ted - insert replies(answer) into database
// MVC
import {RepliesService} from './services/repliesService';
import {RepliesController} from './controllers/repliesController';
const repliesService = new RepliesService(knex);
export const repliesController = new RepliesController(repliesService);

import {CreateAccountService} from './services/createAccountService';
import {CreateAccountController} from './controllers/createAccountController';

const createAccountService = new CreateAccountService(knex);
export const createAccountController = new CreateAccountController(createAccountService);
//Carl createUserAc + Multer


//Carl checkbox for favorite questions
import {CheckBoxService} from './services/checkBoxService';
import {CheckBoxController} from './controllers/checkBoxController';

const checkBoxService = new CheckBoxService(knex);
export const checkBoxController = new CheckBoxController(checkBoxService);

// Carl ProfilePage
import {ProfileService} from './services/profileService';
import {ProfileController} from './controllers/profileController';

const profileService = new ProfileService(knex);
export const profileController = new ProfileController(profileService);
// Carl ProfilePage

//Carl getSearchQuestions
import {SearchQuestionService} from './services/searchQuestionSercive';
import {SearchQuestionController} from './controllers/searchQuestionController';

const searchQuestionService = new SearchQuestionService(knex);
export const searchQuestionController = new SearchQuestionController(searchQuestionService);
//Carl getSearchQuestions

import {routes} from './routes';

app.use('/', routes);
//Carl checkbox for favorite questions





const PORT = process.env.PORT || 8080

app.listen(PORT, () => {
    console.log('Listening at port' + PORT)
})

// const upload = multer({
//     dest: 'uploads/'
// })