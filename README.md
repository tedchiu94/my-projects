# This is my project file
- project2: Job Portal
- project3: Q & A platform(react)

## Job Portal
https://tedsproject.xyz
![alt job_portal](pic/job_portal.jpg)
- Using puppeteer to scrape job posts from different job portal wbsites
![alt search_result](pic/searchresultpage.png)
![alt district](pic/district.png)
![alt salary](pic/salary.png)
![alt source](pic/source.png)
- Recommender system (content base)
![alt recommender_sys](pic/recommender_sys.jpg)
- Chart.js

## Q & A platform(react)
http://tedckh.xyz (back-end is closed)
![alt qa_platform](pic/qa_platform.jpg)
- Seeking help easily and rapidly
- Post and reply question (markdown-format, multer)
![alt ask](pic/ask.jpg)
![alt reply](pic/reply.jpg)
- Score, Bookmark